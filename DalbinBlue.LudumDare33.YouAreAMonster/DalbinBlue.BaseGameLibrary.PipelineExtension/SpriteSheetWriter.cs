﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Dalbinblue.BaseGameLibrary.Data;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentTypeWriter]
  public class SpriteSheetWriter : ContentTypeWriter<SpriteSheet> {
    public override string GetRuntimeReader(TargetPlatform targetPlatform)
    {
      return typeof(SpriteSheetReader).AssemblyQualifiedName;
    }

    public override string GetRuntimeType(TargetPlatform targetPlatform) {
      return typeof(SpriteSheet).AssemblyQualifiedName;
    }

    protected override void Write(ContentWriter output, SpriteSheet value)
    {
      using (var stream = new MemoryStream()) {
        var serializer = new DataContractSerializer(typeof(SpriteSheet));
        serializer.WriteObject(stream, value);
        
        //IFormatter binaryFormatter = new BinaryFormatter();
        //binaryFormatter.Serialize(stream, value);

        output.Write(Convert.ToInt32(stream.Length));
        output.Write(stream.GetBuffer());
      }
    }
  }
}
