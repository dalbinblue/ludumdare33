﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentImporter(".ssf", DefaultProcessor = "SsfContentProcessor", DisplayName = "Custom Sprite Sheet Importer")]
  public class SsfContentImporter : ContentImporter<SsfSpriteSheet> {
    public override SsfSpriteSheet Import(string filename, ContentImporterContext context)
    {
      using (TextReader stream = new StreamReader(filename)) {
        var serializer = new XmlSerializer(typeof(SsfSpriteSheet));
        var spriteSheet = (SsfSpriteSheet)serializer.Deserialize(stream);
        spriteSheet.OriginalFilename = filename;
        return spriteSheet;
      }
    }
  }
}
