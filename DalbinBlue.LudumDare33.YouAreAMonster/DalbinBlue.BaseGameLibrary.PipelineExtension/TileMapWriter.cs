﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Dalbinblue.BaseGameLibrary.Data;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentTypeWriter]
  public class TileMapWriter : ContentTypeWriter<TileMap>{
    public override string GetRuntimeReader(TargetPlatform targetPlatform) {
      return typeof (TileMapReader).AssemblyQualifiedName;
    }

    public override string GetRuntimeType(TargetPlatform targetPlatform) {
      return typeof (TileMap).AssemblyQualifiedName;
    }

    protected override void Write(ContentWriter output, TileMap value) {
      using (var stream = new MemoryStream()) {
        IFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(stream, value);
        output.Write(Convert.ToInt32(stream.Length));
        output.Write(stream.GetBuffer());
      }
    }
  }
}