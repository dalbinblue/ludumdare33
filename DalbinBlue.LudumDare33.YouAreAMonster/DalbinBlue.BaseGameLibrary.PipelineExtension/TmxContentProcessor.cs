using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentProcessor(DisplayName = "TmxContentProcessor")]
  public class TmxContentProcessor : ContentProcessor<TmxMap, TileMap> {
    public override TileMap Process(TmxMap input, ContentProcessorContext context) {
      if (input == null) throw new ArgumentNullException("input");

      var tileMap = new TileMap
                    {
                      HeightInTiles = input.Height,
                      WidthInTiles = input.Width,
                      TileHeight = input.TileHeight,
                      TileWidth = input.TileWidth
                    };

      switch (input.Orientation) {
        case TmxMapOrientation.Isometric:
          tileMap.Orientation = TileMapOrientation.Isometric;
          break;
        case TmxMapOrientation.Orthagonal:
          tileMap.Orientation = TileMapOrientation.Orthogonal;
          break;
        default:
          throw new ArgumentException("Unknown map orientation.");
      }

      tileMap.Properties = ConvertTmxPropertyListToDictionary(input.Properties);

      if (input.Tilesets != null) {
        tileMap.Tilesets = new TileMapTilesetDefinition[input.Tilesets.Count()];
        for (int i = 0; i < input.Tilesets.Count(); i++) {
          tileMap.Tilesets[i] = ConvertTmxTilesetToTileMapTilesetDefinition(input.Tilesets[i],input.OriginalFilename);
        }        
      }
      else {
        tileMap.Tilesets = new TileMapTilesetDefinition[0];
      }
        

      if (input.Layers != null) {
        tileMap.Layers = new TileMapLayer[input.Layers.Count()];
        for (int i = 0; i < input.Layers.Count(); i++) {
          tileMap.Layers[i] = ConvertTmxLayerToTileMapLayer(input.Layers[i]);
        }
      }
      else {
        tileMap.Layers = new TileMapLayer[0];
      }

      if (input.ObjectGroups != null) {
        tileMap.ObjectGroups = new TileMapObjectGroup[input.ObjectGroups.Count()];
        for (int i = 0; i < input.ObjectGroups.Count(); i++) {
          tileMap.ObjectGroups[i] = ConvertTmxObjectGroupToTileMapObjectGroup(input.ObjectGroups[i]);
        }
      }
      else {
        tileMap.ObjectGroups = new TileMapObjectGroup[0];
      }

      return tileMap;
    }

    private static TileMapObjectGroup ConvertTmxObjectGroupToTileMapObjectGroup(TmxObjectGroup input) {
      var group = new TileMapObjectGroup();

      group.Name = input.Name;
      group.Properties = ConvertTmxPropertyListToDictionary(input.Properties);
      
      if (input.Objects != null) {
        group.Objects = new TileMapObject[input.Objects.Length];
        for(int i = 0; i<input.Objects.Length; i++) {
          group.Objects[i] = ConvertTmxObjectToTileMapObject(input.Objects[i]);
        }
      }
      else {
        group.Objects = new TileMapObject[0];        
      }

      return group;
    }

    private static TileMapObject ConvertTmxObjectToTileMapObject(TmxObject input) {
      var mapObject = new TileMapObject
                      {
                        Height = input.Height,
                        IsVisible = (input.Visible == 1),
                        Name = input.Name,
                        Properties = ConvertTmxPropertyListToDictionary(input.Properties),
                        TileId = input.TileId,
                        Type = input.Type,
                        Width = input.Width,
                        X = input.X,
                        Y = input.Y
                      };

      if (input.TileId != 0 && input.TileId != 0xFFFFFFFF) {
        mapObject.GeometryType = TileMapObjectGeometryType.Tile;
      }
      else if (input.Polygon != null) {
        mapObject.GeometryType = TileMapObjectGeometryType.Polygon;
        mapObject.GeometryPoints = ConvertTmxGeometryStringToPointArray(input.Polygon.EncodedPoints);
      }
      else if (input.Polyline != null) {
        mapObject.GeometryType = TileMapObjectGeometryType.PolyLine;
        mapObject.GeometryPoints = ConvertTmxGeometryStringToPointArray(input.Polyline.EncodedPoints);
      }
      else if (input.Width > 0 || input.Height > 0) {
        mapObject.GeometryType = TileMapObjectGeometryType.Rectangle;
      }
      else {
        mapObject.GeometryType = TileMapObjectGeometryType.Point;
      }

      return mapObject;
    }

    private static Point[] ConvertTmxGeometryStringToPointArray(string encodedPoints) {
      var pointStrings = encodedPoints.Split(' ');
      return pointStrings
        .Select(pointString => pointString.Split(','))
        .Select(coordinates => new Point(
                                    Convert.ToInt32(coordinates[0]),
                                    Convert.ToInt32(coordinates[1])))
        .ToArray();
    }

    private static TileMapTilesetDefinition ConvertTmxTilesetToTileMapTilesetDefinition(TmxTileset input, string mapFileOriginalPath) {
      var tileset = new TileMapTilesetDefinition
                    {
                      FirstTileId = input.FirstGid,
                      ImageAssetName = CalculateImageAssetName(input.Image.Source, mapFileOriginalPath),
                      Margin = input.Margin,
                      Name = input.Name,
                      Properties = ConvertTmxPropertyListToDictionary(input.Properties),
                      Spacing = input.Spacing,
                      TileHeight = input.TileHeight,
                      TileWidth = input.TileWidth,
                      TileDetails = new Dictionary<int, TileDetail>()
                    };

      if (input.Tiles != null) {
        foreach (var inputTile in input.Tiles) {
          TileDetail tileDetail = ConvertTmxTileToTileDetail(inputTile);
          tileset.TileDetails.Add(inputTile.Id, tileDetail);
        }
      }
      
      return tileset;
    }

    private static string CalculateImageAssetName(string source, string mapFileOriginalPath) {
        return source.Substring(0,source.LastIndexOf('.'));


        // Just return the name of the image file if an absolute path.
        if (source.Contains(":\\"))
        {
            return Path.GetFileNameWithoutExtension(source);
        }

        List<string> basePath = mapFileOriginalPath.Split('\\').ToList();
        basePath = basePath.Take(basePath.Count() - 1).ToList();

        List<string> sourcePathSegments = source.Split('/').ToList();
        string fileName = sourcePathSegments.Last().Split('.')[0];
        sourcePathSegments.RemoveAt(sourcePathSegments.Count() - 1);

        foreach (var segment in sourcePathSegments)
        {
            if (segment == "..")
            {
                if (basePath.Count() == 0) return fileName;
                basePath.RemoveAt(basePath.Count() - 1);
            }
            else
            {
                basePath.Add(segment);
            }
        }

        // Crappy way to figure out the base directory of the content project, 
        // look for two directories on after another that only differ in name by 
        // the word "Content" at the end.
        bool foundContentPath = false;
        for (int i = 1; i < basePath.Count(); i++)
        {
            if (basePath[i] == (basePath[i - 1] + "Content"))
            {
                basePath = basePath.Skip(i + 1).ToList();
                foundContentPath = true;
            }
        }

        if (!foundContentPath) return fileName;

        var assetName = new StringBuilder();
        foreach (var segment in basePath)
        {
            assetName.Append(segment);
            assetName.Append("\\");
        }
        assetName.Append(fileName);
        return assetName.ToString();
    }

    private static TileDetail ConvertTmxTileToTileDetail(TmxTile inputTile) {
      return new TileDetail {Properties = ConvertTmxPropertyListToDictionary(inputTile.Properties)};
    }

    private static Dictionary<string, string> ConvertTmxPropertyListToDictionary(IEnumerable<TmxProperty> input) {
      return input == null 
        ? new Dictionary<string, string>() 
        : input.ToDictionary(property => property.Name, property => property.Value);
    }

    private static TileMapLayer ConvertTmxLayerToTileMapLayer(TmxLayer input) {
      return new TileMapLayer
                  {
                    Name = input.Name,
                    Properties = ConvertTmxPropertyListToDictionary(input.Properties),
                    LayerTiles = ConvertTmxLayerDataToArray(input.Data, input.Width, input.Height)
                  };
    }

    private static UInt32[][] ConvertTmxLayerDataToArray(TmxLayerData data, int width, int height) {
      var layerMap = new UInt32[width][];
      for (int i = 0; i < width; i++ ) {
        layerMap[i] = new UInt32[height];
      }

      if (data.Encoding != TmxLayerDataEncoding.Base64)
        throw new Exception("Only Tiled Maps encoded in Base64 are supported");

      if (data.Compression != TmxLayerDataCompression.None)
        throw new Exception("Only Tiled Data in an uncompressed form is supported");


      byte[] mapBytes = Convert.FromBase64String(data.EncodedTileData);

      int tileIndex = 0;

      for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
          layerMap[x][y] = 
            Convert.ToUInt32(mapBytes[tileIndex]) |
            Convert.ToUInt32(mapBytes[tileIndex + 1]) << 8 |
            Convert.ToUInt32(mapBytes[tileIndex + 1]) << 16 |
            Convert.ToUInt32(mapBytes[tileIndex + 1]) << 24;

          tileIndex += 4;

        }
      }

      return layerMap;
    }
  }
}