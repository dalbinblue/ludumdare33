﻿using System;
using System.Collections.Generic;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentProcessor(DisplayName = "SsfContentProcessor")]
  public class SsfContentProcessor : ContentProcessor<SsfSpriteSheet,SpriteSheet>
  {
    public override SpriteSheet Process(SsfSpriteSheet input, ContentProcessorContext context)
    {
      if (input == null) throw new ArgumentNullException("input");

      var spriteSheet = new SpriteSheet
                        {
                          ImageAssetName = input.ImageAssetName,
                          ImageSlices = ConvertSsfImageSlicesToSpriteImageSlices(input.ImageSlices),
                          Sprites = ConvertSsfSpritesToSpriteDefinitions(input.Sprites)
                        };
      return spriteSheet;
    }

    private Dictionary<string, SpriteDefinition> ConvertSsfSpritesToSpriteDefinitions(IEnumerable<SsfSprite> inputSprites)
    {
      var sprites = new Dictionary<string, SpriteDefinition>();
      foreach (var inputSprite in inputSprites) {
        var spriteDefinition = ConvertSsfSpriteToSpriteDefinition(inputSprite);
        sprites.Add(inputSprite.SpriteName, spriteDefinition);
      }
      return sprites;
    }

    private SpriteDefinition ConvertSsfSpriteToSpriteDefinition(SsfSprite inputSprite)
    {
      var sprite = new SpriteDefinition
                   {
                     DefaultAnimation = inputSprite.DefaultAnimation,
                     Animations = ConvertSsfAnimationsToSpriteAnimationDefinitions(inputSprite.Animations)
                   };
      return sprite;
    }

    private Dictionary<string, SpriteAnimationDefinition> ConvertSsfAnimationsToSpriteAnimationDefinitions(IEnumerable<SsfAnimation> inputAnimations)
    {
      var animations = new Dictionary<string, SpriteAnimationDefinition>();
      foreach (var inputAnimation in inputAnimations) {
        var animationDefinition = ConvertSsfAnimationToSpriteAnimationDefinition(inputAnimation);
        animations.Add(inputAnimation.AnimationName, animationDefinition);
      }
      return animations;
    }

    private SpriteAnimationDefinition ConvertSsfAnimationToSpriteAnimationDefinition(SsfAnimation inputAnimation)
    {
      var animationDefinition = new SpriteAnimationDefinition
                                {
                                  Frames = ConvertSsfFramesToSpriteFrames(inputAnimation.Frames),
                                  JumpToAnimationOnEnd = inputAnimation.JumpToAnimationOnEnd,
                                  Loop = inputAnimation.Loop,
                                  StartFrame = inputAnimation.StartFrame,
                                  StartFrameOnLoop = inputAnimation.StartFrameOnLoop
                                };
      return animationDefinition;
    }

    private SpriteFrame[] ConvertSsfFramesToSpriteFrames(List<SsfFrame> inputFrames)
    {
      var frames = new SpriteFrame[inputFrames.Count];
      for (var i = 0; i < inputFrames.Count; i++)
      {
        var inputFrame = inputFrames[i];
        frames[i] = ConvertSsfFrameToSpriteFrame(inputFrame);
      }
      return frames;
    }

    private SpriteFrame ConvertSsfFrameToSpriteFrame(SsfFrame inputFrame)
    {
      var frame = new SpriteFrame
                  {
                    FrameLength = inputFrame.FrameLength, 
                    Slice = inputFrame.Slice
                  };
      return frame;
    }

    private static SpriteImageSlice[] ConvertSsfImageSlicesToSpriteImageSlices(List<SsfImageSlice> inputImageSlices)
    {
      var imageSlices = new SpriteImageSlice[inputImageSlices.Count];
      for (int i = 0; i < inputImageSlices.Count; i++)
      {
        var inputSlice = inputImageSlices[i];

        imageSlices[i] = ConvertSsfImageSliceToSpriteImageSlice(inputSlice);
      }
      return imageSlices;
    }

    private static SpriteImageSlice ConvertSsfImageSliceToSpriteImageSlice(SsfImageSlice inputSlice)
    {
      var imageSlice = new SpriteImageSlice
                       {
                         Origin = new Point(inputSlice.OriginX, inputSlice.OriginY),
                         ImageBounds = new Rectangle(inputSlice.X, inputSlice.Y, inputSlice.Width, inputSlice.Height),
                         CollisionBounds =
                           new Rectangle(inputSlice.BoundingBoxX, inputSlice.BoundingBoxY, inputSlice.BoundingBoxWidth,
                                         inputSlice.BoundingBoxHeight)
                       };
      return imageSlice;
    }
  }
}
