using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxProperty {
    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }

    [XmlAttribute(AttributeName = "value")]
    public string Value { get; set; }
  }
}