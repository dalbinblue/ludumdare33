using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public enum TmxMapOrientation {
    [XmlEnum("orthogonal")]
    Orthagonal,
    [XmlEnum("isometric")]
    Isometric
  }
}