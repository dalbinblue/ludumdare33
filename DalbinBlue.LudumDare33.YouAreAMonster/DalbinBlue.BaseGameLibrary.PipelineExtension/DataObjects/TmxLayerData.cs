using System.ComponentModel;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxLayerData {
    [XmlAttribute(AttributeName = "encoding")]
    public TmxLayerDataEncoding Encoding { get; set; }

    [XmlAttribute(AttributeName = "compression")]
    [DefaultValue(typeof(TmxLayerDataCompression),"None")]
    public TmxLayerDataCompression Compression { get; set; }

    [XmlText]
    public string EncodedTileData { get; set; }
  }
}