using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxPolygon {
    [XmlAttribute(AttributeName = "points")]
    public string EncodedPoints { get; set; }
  }
}