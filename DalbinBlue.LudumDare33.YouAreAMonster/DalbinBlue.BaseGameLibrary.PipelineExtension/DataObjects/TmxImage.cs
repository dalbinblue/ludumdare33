using System.ComponentModel;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxImage {
    [XmlAttribute(AttributeName = "source")]
    public string Source { get; set; }

    [XmlAttribute(AttributeName = "trans")]
    public string TransparentColor { get; set; }

    [XmlAttribute(AttributeName = "width")]
    [DefaultValue(0)]
    public int Width { get; set; }

    [XmlAttribute(AttributeName = "heigth")]
    [DefaultValue(0)]
    public int Height { get; set; }
  }
}