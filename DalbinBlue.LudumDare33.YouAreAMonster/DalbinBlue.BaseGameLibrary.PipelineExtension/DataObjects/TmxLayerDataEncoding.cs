using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public enum TmxLayerDataEncoding {
    [XmlEnum("base64")]
    Base64,

    [XmlEnum("csv")]
    Csv
  }
}