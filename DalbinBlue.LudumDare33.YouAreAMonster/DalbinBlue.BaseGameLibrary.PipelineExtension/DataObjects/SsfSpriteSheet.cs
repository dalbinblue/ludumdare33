﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  [Serializable]
  public class SsfSpriteSheet {
    [XmlIgnore]
    public string OriginalFilename { get; set; }

    public string ImageAssetName { get; set; }
    public List<SsfImageSlice> ImageSlices { get; set; }
    public List<SsfSprite> Sprites { get; set; }
  }
}
