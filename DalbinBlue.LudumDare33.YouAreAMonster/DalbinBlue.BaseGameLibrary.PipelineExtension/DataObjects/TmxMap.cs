﻿using System;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {

  [XmlRoot(ElementName = "map")]
  [XmlType(TypeName = "map")]
  public class TmxMap {
    [XmlAttribute(AttributeName = "version")]
    public Decimal Version { get; set; }

    [XmlAttribute(AttributeName = "orientation")]
    public TmxMapOrientation Orientation { get; set; }

    [XmlAttribute(AttributeName = "width")]
    public int Width;

    [XmlAttribute(AttributeName = "height")]
    public int Height;

    [XmlAttribute(AttributeName = "tilewidth")]
    public int TileWidth;

    [XmlAttribute(AttributeName = "tileheight")]
    public int TileHeight;

    [XmlElement(ElementName = "tileset")]
    public TmxTileset[] Tilesets { get; set; }

    [XmlArray(ElementName = "properties")]
    [XmlArrayItem(ElementName = "property")]
    public TmxProperty[] Properties { get; set; }

    [XmlElement(ElementName = "layer")]
    public TmxLayer[] Layers { get; set; }

    [XmlElement(ElementName = "objectgroup")]
    public TmxObjectGroup[] ObjectGroups { get; set; }

    public string OriginalFilename { get; set; }
  }
}
