using System;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects
{
  [Serializable]
  public class SsfFrame {
    public int Slice { get; set; }
    public float FrameLength { get; set; }
  }
}