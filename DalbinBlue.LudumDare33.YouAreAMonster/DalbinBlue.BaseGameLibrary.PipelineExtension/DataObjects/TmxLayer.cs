using System.ComponentModel;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxLayer {

    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }

    [XmlAttribute(AttributeName = "x")]
    [DefaultValue(0)]
    public int X { get; set; }

    [XmlAttribute(AttributeName = "y")]
    [DefaultValue(0)]
    public int Y { get; set; }

    [XmlAttribute(AttributeName = "width")]
    public int Width { get; set; }

    [XmlAttribute(AttributeName = "height")]
    public int Height { get; set; }

    [XmlAttribute(AttributeName = "opacity")]
    [DefaultValue(1.0)]
    public decimal Opacity { get; set; }

    [XmlAttribute(AttributeName = "visible")]
    [DefaultValue(1)]
    public int Visible { get; set; }

    [XmlArray(ElementName = "properties")]
    [XmlArrayItem(ElementName = "property")]
    public TmxProperty[] Properties { get; set; }

    [XmlElement(ElementName = "data")]
    public TmxLayerData Data { get; set; }
  }
}