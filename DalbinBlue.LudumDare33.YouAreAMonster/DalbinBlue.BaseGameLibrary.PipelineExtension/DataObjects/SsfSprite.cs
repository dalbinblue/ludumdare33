using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects
{
  [Serializable]
  public class SsfSprite {
    public string SpriteName { get; set; }
    public List<SsfAnimation> Animations { get; set; }
    public string DefaultAnimation { get; set; }
  }
}