using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxTile {
    [XmlAttribute(AttributeName = "id")]
    public int Id { get; set; }

    [XmlArray(ElementName = "properties")]
    [XmlArrayItem(ElementName = "property")]
    public TmxProperty[] Properties { get; set; }
  }
}