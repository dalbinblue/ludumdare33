using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects
{
  [Serializable]
  public class SsfAnimation {
    public string AnimationName { get; set; }
    public List<SsfFrame> Frames { get; set; }
    public int StartFrame { get; set; }
    public bool Loop { get; set; }
    public string JumpToAnimationOnEnd { get; set; }
    public int StartFrameOnLoop { get; set; }
  }
}