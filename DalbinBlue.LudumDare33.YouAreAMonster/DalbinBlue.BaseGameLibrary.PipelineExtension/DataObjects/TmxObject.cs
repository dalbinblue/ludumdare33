using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxObject {
    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }

    [XmlAttribute(AttributeName = "type")]
    public string Type { get; set; }

    [XmlAttribute(AttributeName = "width")]
    [DefaultValue(0)]
    public int Width { get; set; }

    [XmlAttribute(AttributeName = "height")]
    [DefaultValue(0)]
    public int Height { get; set; }

    [XmlAttribute(AttributeName = "x")]
    [DefaultValue(0)]
    public int X { get; set; }

    [XmlAttribute(AttributeName = "y")]
    [DefaultValue(0)]
    public int Y { get; set; }

    [XmlAttribute(AttributeName = "gid")]
    [DefaultValue(0xFFFFFFFF)]
    public UInt32 TileId { get; set; }

    [XmlAttribute(AttributeName = "visible")]
    [DefaultValue(1)]
    public int Visible { get; set; }

    [XmlArray(ElementName = "properties")]
    [XmlArrayItem(ElementName = "property")]
    public TmxProperty[] Properties { get; set; }

    [XmlElement(ElementName = "polygon")]
    public TmxPolygon Polygon { get; set; }

    [XmlElement(ElementName = "polyline")]
    public TmxPolyline Polyline { get; set; }
  }
}