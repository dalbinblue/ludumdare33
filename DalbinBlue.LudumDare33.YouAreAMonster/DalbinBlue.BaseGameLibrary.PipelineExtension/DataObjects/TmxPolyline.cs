using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxPolyline {
    [XmlAttribute(AttributeName = "points")]
    public string EncodedPoints { get; set; }    
  }
}