using System;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects
{
  [Serializable]
  public class SsfImageSlice {
    public int X { get; set; }
    public int Y { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public int OriginX { get; set; }
    public int OriginY { get; set; }
    public int BoundingBoxX { get; set; }
    public int BoundingBoxY { get; set; }
    public int BoundingBoxWidth { get; set; }
    public int BoundingBoxHeight { get; set; }

    public bool IsPointInSlice(int x, int y) {
      return ((x >= X) && (x < X + Width) && (y >= Y) && (y < Y + Height));
    }
  }
}