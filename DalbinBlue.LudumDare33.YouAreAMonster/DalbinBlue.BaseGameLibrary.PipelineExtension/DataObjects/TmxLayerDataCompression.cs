using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public enum TmxLayerDataCompression {
    None = 0,

    [XmlEnum("gzip")]
    GZip,

    [XmlEnum("zlib")]
    ZLib
  }
}