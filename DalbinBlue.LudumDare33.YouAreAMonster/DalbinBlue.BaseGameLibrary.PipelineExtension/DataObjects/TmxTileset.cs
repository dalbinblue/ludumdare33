using System.ComponentModel;
using System.Xml.Serialization;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects {
  public class TmxTileset {
    [XmlAttribute(AttributeName="firstgid")]
    public int FirstGid { get; set; }

    [XmlAttribute(AttributeName = "source")]
    public string Source { get; set; }

    [XmlAttribute(AttributeName = "tilewidth")]
    public int TileWidth { get; set; }

    [XmlAttribute(AttributeName = "name")]
    public string Name { get; set; }

    [XmlAttribute(AttributeName = "tileheight")]
    public int TileHeight { get; set; }

    [XmlAttribute(AttributeName = "spacing")]
    [DefaultValue(0)]
    public int Spacing { get; set; }

    [XmlAttribute(AttributeName = "margin")]
    [DefaultValue(0)]
    public int Margin { get; set; }

    [XmlElement("image")]
    public TmxImage Image { get; set; }

    [XmlElement("tile", IsNullable = true)]
    public TmxTile[] Tiles { get; set; }

    [XmlArray(ElementName = "properties")]
    [XmlArrayItem(ElementName = "property")]
    public TmxProperty[] Properties { get; set; }
  }
}