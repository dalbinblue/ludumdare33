﻿using System;
using System.Text;
using System.Xml.Serialization;
using Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension.DataObjects;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.IO;

namespace Dalbinblue.BaseGameLibrary.TiledTmxPipelineExtension {
  [ContentImporter(".tmx", DefaultProcessor = "TmxContentProcessor", DisplayName = "Tiled Map Importer")]
  public class TmxContentImporter : ContentImporter<TmxMap> {
    public override TmxMap Import(string filename, ContentImporterContext context) {
      using (TextReader stream = new StreamReader(filename, Encoding.UTF8)) {
        var serializer = new XmlSerializer(typeof(TmxMap));
        var map = (TmxMap)serializer.Deserialize(stream);
        map.OriginalFilename = filename;
        return map;
      }
    }
  }
}
