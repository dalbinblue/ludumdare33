using System.Security.Cryptography.X509Certificates;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public abstract class ActorBase : IPositionedObject
    {
        protected ActorBase(GameEngine engine)
        {
            Engine = engine;
            FacingDirection = CardinalDirection.Down;
        }

        protected GameEngine Engine { get; set; }

        public CardinalDirection FacingDirection { get; set; }
        public AnimatedSprite Sprite { get; set; }
        public virtual Vector2 Position
        {
            get { return Sprite.Position; }
            set { Sprite.Position = value; }
        }

        public virtual void RemoveGraphicsFromScene()
        {
            Engine.ActorLayer.Remove(Sprite);
        }

        public virtual void AddGraphicsToScene()
        {
            Engine.ActorLayer.Add(Sprite);
        }

        public virtual void Update(GameTime gameTime)
        {
            // DO NOTHING
        }

        public virtual Rectangle GetCollisionBounds()
        {
            return Sprite.GetCollisionBounds();
        }
    }
}