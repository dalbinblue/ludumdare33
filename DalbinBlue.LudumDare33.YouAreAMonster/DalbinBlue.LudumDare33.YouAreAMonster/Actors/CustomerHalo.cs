using System;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class CustomerHalo : ActorBase, IUsableActor
    {
        public AnimatedSprite Arrow { get; set; }

        public Building AssociatedBuilding { get; set; }

        public CustomerHalo(GameEngine engine) : base(engine)
        {
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "CustomerHalo", engine.Content);
            Sprite.SetAnimation("Stand");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;

            Arrow = AnimatedSprite.FromSpriteSheet(spriteSheet, "CustomerArrow", engine.Content);
            Arrow.SetAnimation("None");
        }

        public override void Update(GameTime gameTime)
        {
            UpdateArrow();
            base.Update(gameTime);
        }

        private void UpdateArrow()
        {
            Arrow.Position = new Vector2(40,40);

            var playerPosition = Engine.Player.Position;

            var positionDifference = Position - playerPosition;


            if (positionDifference.X < -180)
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(30 - 8, 30 + 8);
                    Arrow.SetAnimation("UpLeft");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(30 - 8, 210 + 8);
                    Arrow.SetAnimation("DownLeft");
                }
                else
                {
                    Arrow.Position = new Vector2(30 - 8, 120 + 8);
                    Arrow.SetAnimation("Left");
                }
            }
            else if (positionDifference.X > 180)
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(290 - 8, 30 + 8);
                    Arrow.SetAnimation("UpRight");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(290 - 8, 210 + 8);
                    Arrow.SetAnimation("DownRight");
                }
                else
                {
                    Arrow.Position = new Vector2(290 - 8, 120 + 8);
                    Arrow.SetAnimation("Right");
                }
            }
            else
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(160 - 8, 30 + 8);
                    Arrow.SetAnimation("Up");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(160 - 8, 210 + 8);
                    Arrow.SetAnimation("Down");
                }
                else
                {
                    Arrow.SetAnimation("None");
                }
            }
        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            Engine.UnhappyCustomerCount += 1;
            RemoveGraphicsFromScene();
            Engine.Actors.Remove(this);
        }

        public override void AddGraphicsToScene()
        {
            Engine.HaloLayer.Add(Sprite);
            Engine.HudLayer.Add(Arrow);
        }

        public override void RemoveGraphicsFromScene()
        {
            Engine.HaloLayer.Remove(Sprite);
            Engine.HudLayer.Remove(Arrow);
        }

        public void Use()
        {
            if (Engine.Player.PizzaCount > 0)
            {
                Engine.CustomerServedCount += 1;
                AssociatedBuilding.AssociatedHalo = null;
                Engine.Player.PizzaCount -= 1;
                RemoveGraphicsFromScene();
                Engine.Actors.Remove(this);
                Engine.SoundManager.PlaySoundEffect("ServeCustomer");
            }
            else
            {
                var sign = new OutOfPizzaSign(Engine);
                sign.Position = Engine.Player.Position.Shift(0, -40f);
                Engine.Actors.Add(sign);
                sign.AddGraphicsToScene();
                Engine.SoundManager.PlaySoundEffect("OutOfPizza");
            }
        }
    }

    public class OutOfPizzaSign : ActorBase
    {
        public OutOfPizzaSign(GameEngine engine) : base(engine)
        {
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "OutOfPizzaSign", engine.Content);
            Sprite.SetAnimation("Stand");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;

        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            RemoveGraphicsFromScene();
            Engine.Actors.Remove(this);
        }

        public override void AddGraphicsToScene()
        {
            Engine.NoticeLater.Add(Sprite);
        }

        public override void RemoveGraphicsFromScene()
        {
            Engine.NoticeLater.Remove(Sprite);
        }

        public override void Update(GameTime gameTime)
        {
            Position = Position.Shift(new Vector2(0, -0.5f));
            base.Update(gameTime);
        }
    }
}