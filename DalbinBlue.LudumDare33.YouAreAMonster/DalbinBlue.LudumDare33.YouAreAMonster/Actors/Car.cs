using System;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class Car : ActorBase, IDestroyableActor
    {
        private CardinalDirection _direction;

        public Car(GameEngine engine, CardinalDirection direction) : base(engine)
        {
            _direction = direction;
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");

            var carNumber = Engine.Random.Next(3) + 1;

            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, $"Car{carNumber}", engine.Content);
            Sprite.SetAnimation("Side");
        }

        public override void Update(GameTime gameTime)
        {
            if (Sprite.CurrentAnimationName == "Destroyed")
            {
                return;
            }

            var playerCollisionBounds = Engine.Player.GetCollisionBounds();
            Rectangle targetBounds = new Rectangle(0,0,0,0);
            Vector2 targetMove;

            switch (_direction)
            {
                case CardinalDirection.None:
                    Sprite.SetAnimation("Side");
                    return;
                case CardinalDirection.Up:
                    targetBounds = new Rectangle((int)Position.X, (int)(Position.Y - 30), 1, 30);
                    Sprite.SetAnimation("Up");
                    targetMove = new Vector2(0f, -1f);
                    break;
                case CardinalDirection.Down:
                    targetBounds = new Rectangle((int)Position.X, (int)Position.Y, 1, 30);
                    Sprite.SetAnimation("Down");
                    targetMove = new Vector2(0f, 1f);
                    break;
                case CardinalDirection.Left:
                    targetBounds = new Rectangle((int) (Position.X - 30), (int) Position.Y,30,1);
                    targetMove = new Vector2(-1f, 0f);
                    Sprite.SetAnimation("Side");
                    Sprite.FlipHorizontally = true;
                    break;
                case CardinalDirection.Right:
                    targetBounds = new Rectangle((int)(Position.X), (int)Position.Y, 30, 1);
                    targetMove = new Vector2(1f, 0f);
                    Sprite.SetAnimation("Side");
                    Sprite.FlipHorizontally = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (!targetBounds.Intersects(playerCollisionBounds))
            {
                Position = Position.Shift(targetMove);

                CheckForMoveTrigger();
            }

            base.Update(gameTime);
        }

        private void CheckForMoveTrigger()
        {
            var currentPoint = Position.ToPoint();
            foreach (var moveTrigger in Engine.MoveTriggers)
            {
                if (currentPoint == moveTrigger.Position)
                {
                    _direction = moveTrigger.Direction;
                }
            }
        }

        public void Destroy()
        {
            Sprite.SetAnimation("Destroyed");
            Engine.DamagePenalty -= 30;
            Engine.Actors.Remove(this);
            Engine.SoundManager.PlaySoundEffect("CarDestroy");

        }
    }
}