namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public interface IDestroyableActor
    {
        void Destroy();
    }
}