namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public interface IUsableActor
    {
        void Use();
    }
}