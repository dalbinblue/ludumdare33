﻿using System;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class Player : ActorBase
    {
        public Player(GameEngine engine) : base(engine)
        {
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "Player", engine.Content);
            PizzaCount = 0;
            MovementState = PlayerMovementState.Standing;

            UpdateAnimation();
        }

        public int PizzaCount { get; set; }
        public PlayerMovementState MovementState { get; set; }


        private void UpdateAnimation()
        {
            string baseAnimationName;
            switch (MovementState)
            {
                case PlayerMovementState.Standing:
                    baseAnimationName = "Stand";
                    break;
                case PlayerMovementState.Walking:
                    baseAnimationName = "Walk";
                    break;
                case PlayerMovementState.Running:
                    baseAnimationName = "Run";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            SetAnimationByDirection(baseAnimationName);
        }

        protected void SetAnimationByDirection(string animationName)
        {
            string directionName;
            switch (FacingDirection)
            {
                case CardinalDirection.Up:
                    directionName = "Up";
                    break;
                case CardinalDirection.Down:
                    directionName = "Down";
                    break;
                case CardinalDirection.Left:
                case CardinalDirection.Right:
                    directionName = "Side";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var fullAnimationName = $"{animationName}{directionName}{(PizzaCount > 0 ? "Pizza" : "NoPizza")}";
            Sprite.SetAnimation(fullAnimationName);
            Sprite.FlipHorizontally = FacingDirection == CardinalDirection.Left;
        }

        public override void Update(GameTime gameTime)
        {
            const float walkSpeed = 2.0f;
            const float runSpeed = 3.0f;

            Vector2 movementVector;
            switch (MovementState)
            {
                case PlayerMovementState.Standing:
                    movementVector = Vector2.Zero;
                    break;
                case PlayerMovementState.Walking:
                    movementVector = GetDirectionalVector(FacingDirection, walkSpeed);
                    break;
                case PlayerMovementState.Running:
                    movementVector = GetDirectionalVector(FacingDirection, runSpeed);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var bounds = Sprite.GetCollisionBounds();

            var collisionResult = Engine.CollisionMap.TestCollision(Sprite.GetCollisionBounds(), movementVector.ToPoint(), false);
            Position = Position.Shift(collisionResult.UncollidingMovementOffset.ToVector2());

            if (collisionResult.HasCollided)
            {
                TryStop();
            }

            UpdateAnimation();

            base.Update(gameTime);

        }

        private Vector2 GetDirectionalVector(CardinalDirection facingDirection, float speed)
        {
            switch (facingDirection)
            {
                case CardinalDirection.None:
                    return Vector2.Zero;
                case CardinalDirection.Up:
                    return new Vector2(0,-speed);
                case CardinalDirection.Down:
                    return new Vector2(0, speed);
                case CardinalDirection.Left:
                    return new Vector2(-speed, 0);
                case CardinalDirection.Right:
                    return new Vector2(speed, 0);
                default:
                    throw new ArgumentOutOfRangeException(nameof(facingDirection), facingDirection, null);
            }
        }

        public void TryStop()
        {
            MovementState = PlayerMovementState.Standing;
        }

        public void TryWalk(CardinalDirection direction)
        {
            MovementState = PlayerMovementState.Walking;
            FacingDirection = direction;
        }

        public void TryRun(CardinalDirection direction)
        {
            MovementState = PlayerMovementState.Running;
            FacingDirection = direction;
        }

        public override Vector2 Position
        {
            get { return Sprite.Position.ToPoint().ToVector2(); }
            set { Sprite.Position = value; }
        }
    }

    public enum PlayerMovementState
    {
        Standing,
        Walking,
        Running
    }
}
