using Dalbinblue.BaseGameLibrary.Audio;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class PizzaHalo : ActorBase, IUsableActor
    {
        public AnimatedSprite Arrow { get; set; }

        public PizzaHalo(GameEngine engine) : base(engine)
        {
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "PizzaHalo", engine.Content);
            Sprite.SetAnimation("Stand");
            Arrow = AnimatedSprite.FromSpriteSheet(spriteSheet, "PizzaIcon", engine.Content);
            Arrow.SetAnimation("None");

        }

        public override void AddGraphicsToScene()
        {
            Engine.HaloLayer.Add(Sprite);
            Engine.HudLayer.Add(Arrow);
        }

        public override void RemoveGraphicsFromScene()
        {
            Engine.HaloLayer.Remove(Sprite);
            Engine.HudLayer.Remove(Arrow);
        }

        public void Use()
        {
            Engine.Player.PizzaCount = 1;
            Engine.SoundManager.PlaySoundEffect("GetPizza");
        }

        public override void Update(GameTime gameTime)
        {
            UpdateArrow();
            base.Update(gameTime);
        }

        private void UpdateArrow()
        {
            Arrow.Position = new Vector2(40, 40);

            var playerPosition = Engine.Player.Position;

            var positionDifference = Position - playerPosition;


            if (positionDifference.X < -180)
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(46 - 8, 46 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(46 - 8, 194 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else
                {
                    Arrow.Position = new Vector2(46 - 8, 120 + 8);
                    Arrow.SetAnimation("Stand");
                }
            }
            else if (positionDifference.X > 180)
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(284 - 8, 46 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(284 - 8, 194 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else
                {
                    Arrow.Position = new Vector2(284 - 8, 120 + 8);
                    Arrow.SetAnimation("Stand");
                }
            }
            else
            {
                if (positionDifference.Y < -120)
                {
                    Arrow.Position = new Vector2(160 - 8, 46 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else if (positionDifference.Y > 120)
                {
                    Arrow.Position = new Vector2(160 - 8, 194 + 8);
                    Arrow.SetAnimation("Stand");
                }
                else
                {
                    Arrow.SetAnimation("None");
                }
            }
        }
    }
}