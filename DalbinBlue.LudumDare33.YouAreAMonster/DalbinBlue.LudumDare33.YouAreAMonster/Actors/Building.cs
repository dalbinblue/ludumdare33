using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class Building : ActorBase, IDestroyableActor
    {
        public CustomerHalo AssociatedHalo;

        private readonly int _buildingNumber;
        public bool IsSpawnPoint { get; private set; }

        public Building(GameEngine engine, int buildingNumber, bool isSpawnPount) : base(engine)
        {
            _buildingNumber = buildingNumber;

            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "Building", engine.Content);
            Sprite.SetAnimation($"Building{buildingNumber}");
            IsSpawnPoint = isSpawnPount;
        }

        public void Destroy()
        {
            if (AssociatedHalo != null)
            {
                AssociatedHalo.RemoveGraphicsFromScene();
                Engine.Actors.Remove(AssociatedHalo);
                Engine.UnhappyCustomerCount += 1;
            }
            Engine.Actors.Remove(this);
            Sprite.SetAnimation($"Destroyed{_buildingNumber}");
            Engine.DamagePenalty -= 100;
            Engine.SoundManager.PlaySoundEffect("BuildingDestroy");
        }
    }
}