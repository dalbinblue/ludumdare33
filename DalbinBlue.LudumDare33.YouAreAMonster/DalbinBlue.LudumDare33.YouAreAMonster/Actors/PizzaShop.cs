using System.Security.Cryptography.X509Certificates;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare33.YouAreAMonster.Actors
{
    public class PizzaShop : ActorBase, IDestroyableActor
    {
        private PizzaHalo _halo;
        private bool _haloActive = false;

        public PizzaShop(GameEngine engine) : base(engine)
        {
            var spriteSheet = engine.Content.Load<SpriteSheet>(@"Sprites");
            Sprite = AnimatedSprite.FromSpriteSheet(spriteSheet, "PizzaShop", engine.Content);
            Sprite.SetAnimation("Stand");

            _halo = new PizzaHalo(engine);
        }

        public override Vector2 Position
        {
            get { return base.Position; }
            set
            {
                base.Position = value;
                _halo.Position = value;
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (Engine.Player.PizzaCount == 0 && !_haloActive)
            {
                _halo.AddGraphicsToScene();
                Engine.Actors.Add(_halo);
                _haloActive = true;
            }
            else if (Engine.Player.PizzaCount > 0 && _haloActive)
            {
                _halo.RemoveGraphicsFromScene();
                Engine.Actors.Remove(_halo);
                _haloActive = false;
            }

            base.Update(gameTime);
        }

        public void Destroy()
        {
            if (_haloActive)
            {
                Engine.Actors.Remove(_halo);
                _halo.RemoveGraphicsFromScene();
            }
            Engine.Actors.Remove(this);
            Sprite.SetAnimation("Destroyed");
            Engine.DamagePenalty -= 100;
        }
    }
}