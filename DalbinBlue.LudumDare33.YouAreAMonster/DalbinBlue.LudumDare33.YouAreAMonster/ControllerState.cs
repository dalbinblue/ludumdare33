﻿using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework.Input;

namespace DalbinBlue.LudumDare33.YouAreAMonster
{
    public class ControllerState : ControllerStateBase
    {
        [ControllerButton("Use", DefaultKey = Keys.LeftAlt, DefaultButton = Buttons.X)]
        public ControllerButtonState Use { get; set; }

        [ControllerButton("Run", DefaultKey = Keys.LeftControl, DefaultButton = Buttons.A)]
        public ControllerButtonState Run { get; set; }
    }
}