﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Audio;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Graphics.Camera;
using Dalbinblue.BaseGameLibrary.Graphics.Hud;
using Dalbinblue.BaseGameLibrary.Input;
using Dalbinblue.BaseGameLibrary.Physics;
using DalbinBlue.LudumDare33.YouAreAMonster.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DalbinBlue.LudumDare33.YouAreAMonster
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameEngine : Game
    {
        GraphicsDeviceManager _graphics;

        public ControllerManager<ControllerState> ControllerManager { get; set; } 

        public RenderEngine RenderEngine { get; set; }
        private TransitionCamera Camera { get; set; }

        public Layer HudLayer { get; set; }
        public Layer HaloLayer { get; set; }
        public Layer ActorLayer { get; set; }
        public Layer NoticeLater { get; set; }
        public Layer BackgroundLayer { get; set; }
        public Layer GameOverLayer { get; set; }
        public Layer HelpCardLayer {get;set;}

        public Player Player { get; set; }
        public BasicTileCollisionMap CollisionMap { get; set; }
        public TileMap Map { get; set; }
        public List<ActorBase> Actors { get; set; }

        public SoundManager SoundManager { get; set; }

        public bool IsGameOver { get; set; }
        public bool IsGameStart { get; set; }
        public IconCounter UnhappyCustomerCounter { get; set; }
        public TextDisplay ScoreDisplay { get; set; }
        public TextDisplay TimeDisplay { get; set; }

        public TextDisplay ScoreShadowDisplay { get; set; }
        public TextDisplay TimeShadowDisplay { get; set; }
        protected SpriteFont Font { get; set; }


        public int CustomerServedCount { get; set; }
        public int DamagePenalty { get; set; }
        
        public int Score => (CustomerServedCount * 200)
                             +DamagePenalty;

        public double SecondsRemaining { get; set; }
        public int UnhappyCustomerCount { get; set; }
        public double SecondsBeforeCustomerSpawn { get; set; }
        public Random Random = new Random();
        public List<MoveTrigger> MoveTriggers = new List<MoveTrigger>();

        public GameEngine()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Initialize Input
            ControllerManager = new ControllerManager<ControllerState>();

            // Initialize Sound
            SoundManager = new SoundManager(Content);
            SoundManager.RegisterMusic("Song","Song");
            SoundManager.RegisterSoundEffect("BuildingDestroy",@"Sound\BuildingDestroy");
            SoundManager.RegisterSoundEffect("CarDestroy", @"Sound\CarDestroy");
            SoundManager.RegisterSoundEffect("GetPizza", @"Sound\GetPizza");
            SoundManager.RegisterSoundEffect("ServeCustomer", @"Sound\ServeCustomer");
            SoundManager.RegisterSoundEffect("OutOfPizza", @"Sound\OutOfPizza");
            SoundManager.PlayMusic("Song");

            // Initialize Graphics
            RenderEngine = new RenderEngine(_graphics, 320, 240, true);
            Camera = new TransitionCamera
            {
                ViewPortWidth = 320,
                ViewPortHeight = 240,
                ClampPositionToScene = true
            };
            RenderEngine.Camera = Camera;

            BackgroundLayer = new Layer();
            RenderEngine.AddLayerToTop(BackgroundLayer);
            HaloLayer = new Layer();
            RenderEngine.AddLayerToTop(HaloLayer);
            ActorLayer = new Layer();
            RenderEngine.AddLayerToTop(ActorLayer);
            NoticeLater = new Layer();
            RenderEngine.AddLayerToTop(NoticeLater);
            HudLayer = new Layer() { IsHudLayer = true, OrderByYCoordinate = false};
            RenderEngine.AddLayerToTop(HudLayer);
            GameOverLayer = new Layer() {IsHudLayer = true, OrderByYCoordinate = false};
            GameOverLayer.Rotation = 0.1f;
            RenderEngine.AddLayerToTop(GameOverLayer);
            HelpCardLayer = new Layer() { IsHudLayer = true, OrderByYCoordinate = false };
            HelpCardLayer.Rotation = 0.1f;
            RenderEngine.AddLayerToTop(HelpCardLayer);
            // Initialize Hud
            Font = Content.Load<SpriteFont>(@"Textures\Font");
            var hudTexture = Content.Load<Texture2D>(@"Textures\Hud");

            UnhappyCustomerCounter = new IconCounter(Font, hudTexture, new Rectangle(0,0,16,16));
            UnhappyCustomerCounter.Position = new Vector2(10,10);
            UnhappyCustomerCounter.MaxRepeatedIconsBeforeShowingNumbers = Int32.MaxValue;
            HudLayer.Add(UnhappyCustomerCounter);

            ScoreDisplay = new TextDisplay(Font, Int32.MaxValue);
            ScoreDisplay.Position = new Vector2(65,10);
            ScoreDisplay.Color = Color.White;
            ScoreShadowDisplay = new TextDisplay(Font, Int32.MaxValue);
            ScoreShadowDisplay.Position = new Vector2(65, 11);
            ScoreShadowDisplay.Color = Color.Black;

            HudLayer.Add(ScoreShadowDisplay);
            HudLayer.Add(ScoreDisplay);

            TimeDisplay = new TextDisplay(Font, Int32.MaxValue);
            TimeDisplay.Position = new Vector2(180, 10);
            TimeDisplay.Color = Color.White;
            TimeShadowDisplay = new TextDisplay(Font, Int32.MaxValue);
            TimeShadowDisplay.Position = new Vector2(180, 11);
            TimeShadowDisplay.Color = Color.Black;

            HudLayer.Add(TimeShadowDisplay);
            HudLayer.Add(TimeDisplay);
            
            // Initialize Engine
            CustomerServedCount = 0;
            DamagePenalty = 0;
            SecondsRemaining = 60*5;
            UnhappyCustomerCount = 0;
            SecondsBeforeCustomerSpawn = 0;
            IsGameStart = true;
            ShowHelpCard();

            Player = new Player(this);
            Player.AddGraphicsToScene();
            Camera.SetTarget(Player);

            Map = Content.Load<TileMap>(@"Map");
            Camera.SceneWidth = Map.WidthInTiles*Map.TileWidth;
            Camera.SceneHeight = Map.HeightInTiles * Map.TileHeight;

            var background = LimitingTileGrid.FromTileMapLayer(Map, Map.GetLayerIndexByName("Background"), Content);

            CollisionMap = BasicTileCollisionMap.FromTileMapLayer(Map, Map.GetLayerIndexByName("Collision"));

            BackgroundLayer.Add(background);

            LoadActors(Map);
        }


        private void LoadActors(TileMap map)
        {
            Actors = new List<ActorBase>();

            var actorLayer = Map.ObjectGroups.First(og => og.Name == "Actors");

            foreach (var actorDefinition in actorLayer.Objects)
            {
                var tileIndex = actorDefinition.TileId;
                var matchingTileSet = Map.Tilesets
                    .Where(ts => ts.FirstTileId <= tileIndex)
                    .OrderByDescending(ts => ts.FirstTileId)
                    .First();

                var actorId = (ActorIdentifiers)(tileIndex - matchingTileSet.FirstTileId);
                var actorPosition = new Vector2(actorDefinition.X + 8, actorDefinition.Y - 1);

                switch (actorId)
                {
                    case ActorIdentifiers.PlayerStartPosition:
                        Player.Position = actorPosition;
                        break;
                    case ActorIdentifiers.PizzaShop:
                        var pizzaShop = new PizzaShop(this);
                        pizzaShop.Position = actorPosition;
                        pizzaShop.AddGraphicsToScene();
                        Actors.Add(pizzaShop);
                        break;
                    case ActorIdentifiers.CarStopped:
                        CreateCar(CardinalDirection.None,actorPosition);
                        break;
                    case ActorIdentifiers.CarUp:
                        CreateCar(CardinalDirection.Up, actorPosition);
                        break;
                    case ActorIdentifiers.CarDown:
                        CreateCar(CardinalDirection.Down, actorPosition);
                        break;
                    case ActorIdentifiers.CarLeft:
                        CreateCar(CardinalDirection.Left, actorPosition);
                        break;
                    case ActorIdentifiers.CarRight:
                        CreateCar(CardinalDirection.Right, actorPosition);
                        break;
                    case ActorIdentifiers.MoveTriggerUp:
                        CreateMoveTrigger(CardinalDirection.Up, actorPosition);
                        break;
                    case ActorIdentifiers.MoveTriggerDown:
                        CreateMoveTrigger(CardinalDirection.Down, actorPosition);
                        break;
                    case ActorIdentifiers.MoveTriggerLeft:
                        CreateMoveTrigger(CardinalDirection.Left, actorPosition);
                        break;
                    case ActorIdentifiers.MoveTriggerRight:
                        CreateMoveTrigger(CardinalDirection.Right, actorPosition);
                        break;
                    default:
                        if (actorId >= ActorIdentifiers.FirstBuilding && actorId <= ActorIdentifiers.LastBuilding)
                        {
                            var buildingId = (actorId - ActorIdentifiers.FirstBuilding) / 2;
                            var isSpawnPoint = (actorId - ActorIdentifiers.FirstBuilding) % 2 == 1;
                            var building = new Building(this,buildingId,isSpawnPoint);
                            building.Position = actorPosition;
                            building.AddGraphicsToScene();
                            Actors.Add(building);
                        }
                        else
                        {
                            throw new ArgumentOutOfRangeException();
                        }
                        break;
                }
            }
        }

        private void CreateMoveTrigger(CardinalDirection direction, Vector2 actorPosition)
        {
            MoveTriggers.Add(new MoveTrigger()
            {
                Direction = direction,
                Position = actorPosition.ToPoint().Shift(0,-2)
            });
        }

        private void CreateCar(CardinalDirection direction, Vector2 position)
        {
            var car = new Car(this, direction);
            car.Position = position.Shift(0,-2);
            car.AddGraphicsToScene();
            Actors.Add(car);
        }

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// game-specific content.
    /// </summary>
    protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (IsGameStart)
            {
                HandleGameStartInput(gameTime);
            }
            else if (!IsGameOver)
            {
                SecondsRemaining -= gameTime.ElapsedGameTime.TotalSeconds;
                HandleInput(gameTime);
                Player.Update(gameTime);
                UpdateActors(gameTime);
                UpdateCustomerSpawner(gameTime);
                CheckForGameOver();
            }

            UpdateHud(gameTime);
            RenderEngine.Update(gameTime);
            
            base.Update(gameTime);
        }

        private void HandleGameStartInput(GameTime gameTime)
        {
            var state = ControllerManager.Update(gameTime);

            if (state.Run.IsDown || state.Use.IsDown)
            {
                HelpCardLayer.AnimatePosition(new Vector2(320, 0), 1.0f, Easing.QuadraticOutEasing);
                IsGameStart = false;
            }
        }

        private void CheckForGameOver()
        {
            if (SecondsRemaining <= 0)
            {
                HandleGameOver();   
            }

            else if (UnhappyCustomerCount >= 3)
            {
                HandleGameOver();
            }
        }

        private void HandleGameOver()
        {
            if (Score < 0 || UnhappyCustomerCount >= 3)
            {
                ShowPinkSlip();
            }
            else
            {
                ShowPayCheck();
            }
            IsGameOver = true;
        }

        private void ShowPayCheck()
        {
            var chequeTexture = Content.Load<Texture2D>(@"Textures\Cheque");
            var chequeBackground = new StaticImage(chequeTexture);
            chequeBackground.Position = new Vector2(40, 62);
            GameOverLayer.Add(chequeBackground);

            var profitText = new TextDisplay(Font, 220);
            profitText.Position = new Vector2(40 + 15, 64 + 47);
            profitText.Color = Color.Black;
            profitText.Text = $"Profit:  ${CustomerServedCount * 200}";
            GameOverLayer.Add(profitText);

            var damageText = new TextDisplay(Font, 220);
            damageText.Position = new Vector2(40 + 15, 64 + 47 + 10);
            damageText.Color = Color.Black;
            damageText.Text = $"Damage: -${0 - DamagePenalty}";
            GameOverLayer.Add(damageText);

            var totalText = new TextDisplay(Font, 220);
            totalText.Position = new Vector2(40 + 15, 64 + 47 + 20);
            totalText.Color = Color.Black;
            var totalSign = Score < 0 ? "-" : " ";
            totalText.Text = $"Total:  {totalSign}${Math.Abs(Score)}";
            GameOverLayer.Add(totalText);

            GameOverLayer.Position = new Vector2(-320, 0);
            GameOverLayer.AnimatePosition(new Vector2(0, 0), 1.0f, Easing.QuadraticOutEasing);
        }

        private void ShowHelpCard()
        {
            var texture = Content.Load<Texture2D>(@"Textures\HelpCard");
            var background = new StaticImage(texture);
            background.Position = new Vector2(30, 30);
            HelpCardLayer.Add(background);
            HelpCardLayer.Position = new Vector2(-320, 0);
            HelpCardLayer.AnimatePosition(new Vector2(0, 0), 1.0f, Easing.QuadraticOutEasing);
        }

        private void ShowPinkSlip()
        {
            var pinkSlipTexture = Content.Load<Texture2D>(@"Textures\PinkSlip");
            var pinkSlipBackground = new StaticImage(pinkSlipTexture);
            pinkSlipBackground.Position = new Vector2(40,62);
            GameOverLayer.Add(pinkSlipBackground);

            var reasonText = new TextDisplay(Font,220);
            reasonText.Position = new Vector2(40+10,62+42);
            reasonText.Color = Color.Black;

            if (UnhappyCustomerCount >= 3)
            {
                reasonText.Text = " Reason: Customer compaints due to undelivered pizza.";
            }
            else
            {
                reasonText.Text = "Reason: Collateral damage costs exceed profit.";
            }

            GameOverLayer.Add(reasonText);

            var profitText = new TextDisplay(Font,220);
            profitText.Position = new Vector2(40+10, 64+42+20);
            profitText.Color = Color.Black;
            profitText.Text = $"Profit:  ${CustomerServedCount*200}";
            GameOverLayer.Add(profitText);

            var damageText = new TextDisplay(Font, 220);
            damageText.Position = new Vector2(40 + 10, 64 + 42 + 30);
            damageText.Color = Color.Black;
            damageText.Text = $"Damage: -${0-DamagePenalty}";
            GameOverLayer.Add(damageText);

            var totalText = new TextDisplay(Font, 220);
            totalText.Position = new Vector2(40 + 10, 64 + 42 + 40);
            totalText.Color = Color.Black;
            var totalSign = Score < 0 ? "-" : " ";
            totalText.Text = $"Total:  {totalSign}${Math.Abs(Score)}";
            GameOverLayer.Add(totalText);

            GameOverLayer.Position = new Vector2(-320,0);
            GameOverLayer.AnimatePosition(new Vector2(0,0), 1.0f, Easing.QuadraticOutEasing);
        }


        private void UpdateHud(GameTime gameTime)
        {
            UnhappyCustomerCounter.Value = UnhappyCustomerCount;

            ScoreDisplay.Text = $"Score: {Score}";
            ScoreShadowDisplay.Text = $"Score: {Score}";

            var totalSecondsLeft = (int) SecondsRemaining;
            var minutesLeft = totalSecondsLeft/60;
            var secondsLeft = totalSecondsLeft%60;
            var extraZero = secondsLeft < 10 ? "0" : string.Empty;
            TimeDisplay.Text = $"Time: {minutesLeft}:{extraZero}{secondsLeft}";
            TimeShadowDisplay.Text = $"Time: {minutesLeft}:{extraZero}{secondsLeft}";
        }

        private void UpdateCustomerSpawner(GameTime gameTime)
        {
            SecondsBeforeCustomerSpawn -= gameTime.ElapsedGameTime.TotalSeconds;
            if (SecondsBeforeCustomerSpawn < 0)
            {
                SecondsBeforeCustomerSpawn = 15;
                SpawnCustomer();
            }
            else if (!Actors.OfType<CustomerHalo>().Any())
            {
                SpawnCustomer();
            }
        }

        private List<Building> _usedSpawners = new List<Building>();

        private void SpawnCustomer()
        {
            var candidateSpawners = Actors.OfType<Building>().Where(b => b.IsSpawnPoint && !_usedSpawners.Contains(b)).ToList();

            if (candidateSpawners.Any())
            {
                int index = Random.Next(candidateSpawners.Count);
                var building = candidateSpawners[index];
                _usedSpawners.Add(building);

                var halo = new CustomerHalo(this);
                building.AssociatedHalo = halo;
                halo.AssociatedBuilding = building;
                halo.Position = building.Position;
                halo.AddGraphicsToScene();
                Actors.Add(halo);
            }
            else
            {
                IsOutOfCustomers = true;
                HandleGameOver();
            }
        }

        public bool IsOutOfCustomers { get; set; }

        private void UpdateActors(GameTime gameTime)
        {
            var playerCollisionBounds = Player.GetCollisionBounds();

            var currentActors = new List<ActorBase>(Actors);

            foreach (var actor in currentActors)
            {
                actor.Update(gameTime);
                var destroyableActor = actor as IDestroyableActor;

                if (destroyableActor != null)
                {
                    var bounds = actor.GetCollisionBounds();
                    if (playerCollisionBounds.Intersects(bounds))
                    {
                       destroyableActor.Destroy();
                    }
                }
            }
        }

        private void HandleInput(GameTime gameTime)
        {
            var state = ControllerManager.Update(gameTime);

            if (state.Down.IsDown)
            {
                if (state.Run.IsDown)
                {
                    Player.TryRun(CardinalDirection.Down);
                }
                else
                {
                    Player.TryWalk(CardinalDirection.Down);
                }
            }
            else if (state.Up.IsDown)
            {
                if (state.Run.IsDown)
                {
                    Player.TryRun(CardinalDirection.Up);
                }
                else
                {
                    Player.TryWalk(CardinalDirection.Up);
                }
            }
            else if (state.Left.IsDown)
            {
                if (state.Run.IsDown)
                {
                    Player.TryRun(CardinalDirection.Left);
                }
                else
                {
                    Player.TryWalk(CardinalDirection.Left);
                }
            }
            else if (state.Right.IsDown)
            {
                if (state.Run.IsDown)
                {
                    Player.TryRun(CardinalDirection.Right);
                }
                else
                {
                    Player.TryWalk(CardinalDirection.Right);
                }
            }
            else
            {
                Player.TryStop();
            }

            if (state.Use.IsJustPressed)
            {
                TryUseObjectAtCurrentLocation();
            }
        }

        private void TryUseObjectAtCurrentLocation()
        {
            var playerCollisionBounds = Player.GetCollisionBounds();
            playerCollisionBounds.Inflate(4,4);


            var currentActors = new List<ActorBase>(Actors);
            foreach (var actor in currentActors)
            {
                var bounds = actor.GetCollisionBounds();
                if (playerCollisionBounds.Intersects(bounds))
                {
                    var usableActor = actor as IUsableActor;
                    if (usableActor != null)
                    {
                        usableActor.Use();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            RenderEngine.Draw(gameTime);
            base.Draw(gameTime);
        }
    }

    public class MoveTrigger
    {
        public Point Position { get; set; }
        public CardinalDirection Direction { get; set; }
    }

    public enum ActorIdentifiers
    {
        PlayerStartPosition = 0,
        PizzaShop = 1,
        FirstBuilding = 2,
        LastBuilding = 25,
        CarStopped = 26,
        CarUp = 27,
        CarDown = 28,
        CarLeft = 29,
        CarRight = 30,
        MoveTriggerUp = 32,
        MoveTriggerDown = 33,
        MoveTriggerLeft = 34,
        MoveTriggerRight = 35
    }
}
