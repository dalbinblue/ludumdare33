﻿namespace Dalbinblue.BaseGameLibrary.Scripting
{
  public class ScriptState
  {
    public bool IsComplete { get; set; }
    public object ReturnValue { get; set; }
  }
}
