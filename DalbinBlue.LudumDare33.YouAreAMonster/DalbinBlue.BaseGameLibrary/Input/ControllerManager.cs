﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Dalbinblue.BaseGameLibrary.Input {
  public class ControllerManager<T> where T : ControllerStateBase, new() {
    private Dictionary<string, ControllerButtonSettings> _buttons;
    private Dictionary<string, ControllerButtonState> _buttonStates;

    public ControllerManager()
    {
      _buttons = new Dictionary<string, ControllerButtonSettings>();
      _buttonStates = new Dictionary<string, ControllerButtonState>();

      Type controllerStateType = typeof (T);

      var properties = controllerStateType.GetProperties();
      foreach (var property in properties)
      {
        var attributes = property.GetCustomAttributes(false);
        foreach (var attribute in attributes)
        {
          var controllerButtonAttribute = attribute as ControllerButtonAttribute;
          if (controllerButtonAttribute != null)
          {
            string buttonName = controllerButtonAttribute.ButtonName;
            var settings = new ControllerButtonSettings();

            if (controllerButtonAttribute.DefaultButton != Buttons.BigButton)
            {
              settings.MappedControllerButtons.Add(controllerButtonAttribute.DefaultButton);
            }

            if (controllerButtonAttribute.DefaultKey != Keys.None)
            {
              settings.MappedKeyboardKeys.Add(controllerButtonAttribute.DefaultKey);
            }

            settings.RepeatButtonPressOnHold = controllerButtonAttribute.RepeatButtonPressOnHold;
            settings.SecondsPerButtonPressRepeat = controllerButtonAttribute.SecondsPerButtonPress;
            settings.DoubleTapThreshold = controllerButtonAttribute.DoubleTapThreshold;

            AddButton(buttonName,settings);
          }
        }
      }

    }

    public void AddButton(string buttonName) {
      AddButton(buttonName, CreateDefaultButtonSettings());
    }

    private ControllerButtonSettings CreateDefaultButtonSettings() {
      return new ControllerButtonSettings {
        RepeatButtonPressOnHold = false,
        SecondsPerButtonPressRepeat = ControllerButtonSettings.DefaultSecondsPerButtonPressRepeat,
        DoubleTapThreshold = ControllerButtonSettings.DefaultDoubleTapThreshold
      };
    }

    public void AddButton(string buttonName, ControllerButtonSettings settings) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonAlreadyExistsException(buttonName);
      }

      _buttons.Add(buttonName, settings);
      _buttonStates.Add(buttonName, CreateInitialButtonState());
    }

    private ControllerButtonState CreateInitialButtonState() {
      ControllerButtonState state = new ControllerButtonState();
      state.Position = ControllerButtonPosition.Up;
      state.RepeatCount = 0;
      state.SecondsSinceButtonPressed = 0.0;
      state.SecondsSinceLastRepeat = 0.0;
      state.TimeOfPress = null;
      return state;
    }

    public void RemoveButton(string buttonName) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (_buttons.ContainsKey(buttonName)) {
        _buttons.Remove(buttonName);
        _buttonStates.Remove(buttonName);
      }
    }

    public void AddControllerButtonMapping(string buttonName, Buttons button) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      if (!settings.MappedControllerButtons.Contains(button)) {
        settings.MappedControllerButtons.Add(button);
      }
    }

    public void RemoveControllerButtonMapping(string buttonName, Buttons button) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      if (settings.MappedControllerButtons.Contains(button)) {
        settings.MappedControllerButtons.Remove(button);
      }
    }

    public void ClearControllerButtonMappings(string buttonName) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      settings.MappedControllerButtons.Clear();
    }

    public void AddKeyboardKeyMapping(string buttonName, Keys key) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      if (!settings.MappedKeyboardKeys.Contains(key)) {
        settings.MappedKeyboardKeys.Add(key);
      }
    }

    public void RemoveKeyboardKeyMappings(string buttonName, Keys key) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      if (settings.MappedKeyboardKeys.Contains(key)) {
        settings.MappedKeyboardKeys.Remove(key);
      }
    }

    public void ClearKeyboardKeyMappings(string buttonName) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      settings.MappedKeyboardKeys.Clear();
    }

    public void ClearAllMappings(string buttonName) {
      ClearControllerButtonMappings(buttonName);
      ClearKeyboardKeyMappings(buttonName);
    }

    public IEnumerable<string> ListButtons() {
      return _buttons.Keys;
    }

    public IEnumerable<Buttons> GetMappedControllerButtons(string buttonName) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      return settings.MappedControllerButtons;
    }

    public IEnumerable<Keys> GetMappedKeyboardKeys(string buttonName) {
      if (buttonName == null) {
        throw new ArgumentNullException("buttonName");
      }

      if (!_buttons.ContainsKey(buttonName)) {
        throw new ControllerButtonNotDefinedException(buttonName);
      }

      ControllerButtonSettings settings = _buttons[buttonName];
      return settings.MappedKeyboardKeys;
    }

    public IEnumerable<Buttons> GetUsedControllerButtons() {
      HashSet<Buttons> mappedButtons = new HashSet<Buttons>();
      foreach (ControllerButtonSettings controllerButtonSettings in _buttons.Values) {
        foreach (Buttons mappedControllerButton in controllerButtonSettings.MappedControllerButtons) {
          if (!mappedButtons.Contains(mappedControllerButton)) {
            mappedButtons.Add(mappedControllerButton);
          }
        }
      }
      return mappedButtons;
    }

    public IEnumerable<Keys> GetUsedKeyboardKeys() {
      HashSet<Keys> mappedKeys = new HashSet<Keys>();
      foreach (ControllerButtonSettings controllerButtonSettings in _buttons.Values) {
        foreach (Keys mappedKeyboardKey in controllerButtonSettings.MappedKeyboardKeys) {
          if (!mappedKeys.Contains(mappedKeyboardKey)) {
            mappedKeys.Add(mappedKeyboardKey);
          }
        }
      }
      return mappedKeys;
    }

    public T Update(GameTime gameTime) {
      GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
      KeyboardState keyboardState = Keyboard.GetState();

      var newButtonState = new Dictionary<string, ControllerButtonState>();
      foreach (string buttonName in _buttons.Keys) {
        newButtonState.Add(buttonName, CalculateCurrentButtonState(buttonName, gamepadState, keyboardState, gameTime));
      }
      _buttonStates = newButtonState;
      return GetControllerState();
    }

    private ControllerButtonState CalculateCurrentButtonState(string buttonName, GamePadState gamepadState, KeyboardState keyboardState, GameTime gameTime) {
      ControllerButtonState previousState = _buttonStates[buttonName];
      ControllerButtonState newState = new ControllerButtonState();
      ControllerButtonSettings settings = _buttons[buttonName];

      bool isButtonDown = false;

      foreach (Buttons button in settings.MappedControllerButtons) {
        if (!gamepadState.IsButtonDown(button)) continue;
        isButtonDown = true;
        break;
      }

      if (!isButtonDown) {
        foreach (Keys key in settings.MappedKeyboardKeys) {
          if (!keyboardState.IsKeyDown(key)) continue;
          isButtonDown = true;
          break;
        }
      }

      bool wasButtonDownInPreviousState = previousState.IsDown;

      newState.Position = CalculateNewPosition(isButtonDown, wasButtonDownInPreviousState);

      if (isButtonDown) {
        if (settings.RepeatButtonPressOnHold) {
          newState.RepeatCount = previousState.RepeatCount;
          newState.SecondsSinceLastRepeat =
            previousState.SecondsSinceLastRepeat + gameTime.ElapsedGameTime.TotalSeconds;

          while (newState.SecondsSinceLastRepeat > settings.SecondsPerButtonPressRepeat) {
            newState.SecondsSinceLastRepeat -= settings.SecondsPerButtonPressRepeat;
            newState.Position |= ControllerButtonPosition.JustPressed;
            newState.RepeatCount += 1;
          }
        }
        else {
          newState.RepeatCount = 0;
          newState.SecondsSinceLastRepeat = 0.0;
        }


        if (wasButtonDownInPreviousState)
        {
          newState.SecondsSinceButtonPressed = previousState.SecondsSinceButtonPressed +
                                               gameTime.ElapsedGameTime.TotalSeconds;          
        }
        else
        {
          newState.SecondsSinceButtonPressed = 0.0;
        }

        newState.TimeOfPress =
          wasButtonDownInPreviousState
          ? previousState.TimeOfPress
          : gameTime;

        if (newState.IsJustPressed && previousState.SecondsSinceButtonPressed > 0.02 && previousState.SecondsSinceButtonPressed < settings.DoubleTapThreshold)
        {
          newState.IsDoubleTap = true;
        }
        else
        {
          newState.IsDoubleTap = false;
        }

      }
      else {
        newState.RepeatCount = 0;
        newState.SecondsSinceButtonPressed = previousState.SecondsSinceButtonPressed + gameTime.ElapsedGameTime.TotalSeconds;
        newState.SecondsSinceLastRepeat = 0.0;
        newState.TimeOfPress = null;
      }




      return newState;
    }

    private ControllerButtonPosition CalculateNewPosition(bool isButtonDown, bool wasButtonDownInPreviousState) {
      if (isButtonDown) {
        return wasButtonDownInPreviousState
                 ? ControllerButtonPosition.Down
                 : ControllerButtonPosition.Down | ControllerButtonPosition.JustPressed;
      }
      return wasButtonDownInPreviousState
               ? ControllerButtonPosition.Up | ControllerButtonPosition.JustReleased
               : ControllerButtonPosition.Up;
    }

    public T GetControllerState() {
      var controllerState = new T();

      var properties = typeof(T).GetProperties();
      foreach (var property in properties) {
        var attributes = property.GetCustomAttributes(false);
        foreach (var attribute in attributes) {
          var controllerButtonAttribute = attribute as ControllerButtonAttribute;
          if (controllerButtonAttribute != null) {
            string buttonName = controllerButtonAttribute.ButtonName;

            property.SetValue(controllerState, _buttonStates[buttonName],null);
          }
        }
      }

      return controllerState;
    }
  }
}
