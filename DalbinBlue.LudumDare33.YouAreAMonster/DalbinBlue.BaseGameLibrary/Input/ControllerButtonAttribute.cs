using System;
using Microsoft.Xna.Framework.Input;

namespace Dalbinblue.BaseGameLibrary.Input
{
  [AttributeUsage(AttributeTargets.Property)]
  public class ControllerButtonAttribute : Attribute
  {
    public string ButtonName { get; protected set; }

    public Keys DefaultKey { get; set; }
    public Buttons DefaultButton { get; set; }
    public bool RepeatButtonPressOnHold { get; set; }
    public double SecondsPerButtonPress { get; set; }
    public double DoubleTapThreshold { get; set; }

    public ControllerButtonAttribute(string buttonName)
    {
      DefaultKey = Keys.None;
      DefaultButton = Buttons.BigButton; // A bit of a hack since there is no "None" option.
      ButtonName = buttonName;
      SecondsPerButtonPress = ControllerButtonSettings.DefaultSecondsPerButtonPressRepeat;
      DoubleTapThreshold = ControllerButtonSettings.DefaultDoubleTapThreshold;
    }
  }
}