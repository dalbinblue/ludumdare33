﻿

using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace Dalbinblue.BaseGameLibrary.Audio {
  public class SoundManager : IEngineComponent {
    private readonly SoundManagerOptions _options;
    private readonly ContentManager _contentManager;
    private readonly Dictionary<string, MusicItem> _musicItems;
    private readonly Dictionary<string, SoundEffectItem> _soundEffectItems;
    private readonly List<PlayingSoundEffectInstance> _soundEffectsPlaying; 

    private Song _currentMusic;
    private Action _callBackWhenMusicFinished;
    private string _nextMusicName;

    public SoundManager(ContentManager contentManager) : this(contentManager, SoundManagerOptions.DefaultSoundManagerOptions)
    {
      // No additional implementation needed
    }

    public SoundManager(ContentManager contentManager, SoundManagerOptions options)
    {
      _contentManager = contentManager;
      _options = options;

      _musicItems = new Dictionary<string, MusicItem>();
      _soundEffectItems = new Dictionary<string, SoundEffectItem>();
      _soundEffectsPlaying = new List<PlayingSoundEffectInstance>();
    }

    public void RegisterMusic(string name, string assetName)
    {
      RegisterMusic(name, assetName, MusicAssetOptions.DefaultMusicAssetOptions);
    }

    public void RegisterMusic(string name, string assetName, MusicAssetOptions options)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (assetName == null) throw new ArgumentNullException("assetName");
      if (options == null) throw new ArgumentNullException("options");
      if (_musicItems.ContainsKey(name)) 
        throw new ArgumentException(
          string.Format("Music '{0}' already registered,", name),
          "name");

      var item = new MusicItem
                 {
                   Name = name,
                   AssetName = assetName,
                   Options = options
                 };

      _musicItems.Add(name,item);
    }

    public void RegisterSoundEffect(string name, string assetName)
    {
      RegisterSoundEffect(name, assetName, SoundEffectOptions.DefaultSoundEffectOptions);
    }

    public void RegisterSoundEffect(string name, string assetName, SoundEffectOptions options)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (assetName == null) throw new ArgumentNullException("assetName");
      if (options == null) throw new ArgumentNullException("options");
      if (_soundEffectItems.ContainsKey(name))
        throw new ArgumentException(
          string.Format("Sound effect '{0}' already registered,", name),
          "name");

      var item = new SoundEffectItem
                 {
                   Name = name,
                   AssetName = assetName,
                   Options = options
                 };

      if (options.KeepSoundEffectInMemory)
      {
        item.SoundEffect = _contentManager.Load<SoundEffect>(assetName);
      }

      item.Instances = new PlayingSoundEffectInstance[_options.MaximumSoundEffectInstances];

      _soundEffectItems.Add(name,item);
    } 

    public ScriptState PlaySoundEffect(string name)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (_soundEffectItems.ContainsKey(name) == false)
        throw new ArgumentException(
          string.Format("No sound effect with key '{0}' registered.", name),
          "name");

      SoundEffectItem item = _soundEffectItems[name];

      return PlaySoundEffect(name, item.Options.SoundEffectPlaybackMethod, null);
    }

    public ScriptState PlaySoundEffect(string name, SoundEffectPlaybackMethod playbackMethod)
    {
      return PlaySoundEffect(name, playbackMethod, null);
    }

    public ScriptState PlaySoundEffect(string name, SoundEffectPlaybackMethod playbackMethod, Action playingFinishedCallback)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (_soundEffectItems.ContainsKey(name) == false)
        throw new ArgumentException(
          string.Format("No sound effect with key '{0}' registered.", name),
          "name");

      SoundEffectItem item = _soundEffectItems[name];

      if (item.SoundEffect == null)
      {
        item.SoundEffect = _contentManager.Load<SoundEffect>(item.AssetName);        
      }

      PlayingSoundEffectInstance instance = null;

      switch (playbackMethod)
      {
        case SoundEffectPlaybackMethod.PlayAdditionalCopy:

          for (int i = 0; i < _options.MaximumSoundEffectInstances; i++) {
            if (item.Instances[i] == null)
            {
              item.Instances[i] = new PlayingSoundEffectInstance();
              instance = item.Instances[i];
              break;
            }
            if (item.Instances[i].SoundEffectInstance == null)
            {
              instance = item.Instances[i];
              break;
            }
            if (item.Instances[i].SoundEffectInstance.State != SoundState.Playing)
            {
              instance = item.Instances[i];
              break;              
            }
          }
          break;
        case SoundEffectPlaybackMethod.RestartIfPlaying:

          if (item.Instances[0] == null)
          {
            item.Instances[0] = new PlayingSoundEffectInstance();
          }

          instance = item.Instances[0];

          break;
        case SoundEffectPlaybackMethod.PlayIfNotAlreadyPlaying:

          bool isPlaying = false;

          for (int i = 0; i < _options.MaximumSoundEffectInstances; i++)
          {
            if (item.Instances[i] == null) continue;
            if (item.Instances[i].SoundEffectInstance == null) continue;
            if (item.Instances[i].SoundEffectInstance.State == SoundState.Playing)
            {
              isPlaying = true;
            }
            else
            {
              instance = item.Instances[i];
            }           
          }

          if (!isPlaying && instance == null)
          {
            item.Instances[0] = new PlayingSoundEffectInstance();
            instance = item.Instances[0];
          }

          break;
      }

      if (instance == null) return new ScriptState {IsComplete = true};

      instance.CallbackWhenSoundEffectFinished = playingFinishedCallback;

      if (instance.ScriptWaitingForSoundEffectToFinish != null)
      {
        instance.ScriptWaitingForSoundEffectToFinish.IsComplete = true;
      }
      instance.ScriptWaitingForSoundEffectToFinish = new ScriptState();
      
      if (instance.SoundEffectInstance == null)
      {
        instance.SoundEffectInstance = item.SoundEffect.CreateInstance();
      }
      else if (instance.SoundEffectInstance.State == SoundState.Playing ||
            instance.SoundEffectInstance.State == SoundState.Paused)
      {
        instance.SoundEffectInstance.Stop();
      }   

      instance.SoundEffectInstance.Play();

      _soundEffectsPlaying.Add(instance);

      return instance.ScriptWaitingForSoundEffectToFinish;
    }

    public void CancelSoundEffect(string name)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (_soundEffectItems.ContainsKey(name) == false)
        throw new ArgumentException(
          string.Format("No sound effect with key '{0}' registered.", name),
          "name");

      SoundEffectItem item = _soundEffectItems[name];

      for (int i = 0; i < _options.MaximumSoundEffectInstances; i++)
      {
        if (item.Instances[i] == null) continue;
        if (item.Instances[i].SoundEffectInstance == null) continue;

        if (item.Instances[i].ScriptWaitingForSoundEffectToFinish != null)
        {
          item.Instances[i].ScriptWaitingForSoundEffectToFinish.IsComplete = true;
        }

        if (item.Instances[i].SoundEffectInstance.State != SoundState.Stopped)
        {
          item.Instances[i].SoundEffectInstance.Stop();
          item.Instances[i].CallbackWhenSoundEffectFinished = null;

          if (!item.Options.KeepSoundEffectInMemory)
          {
            item.Instances[i].SoundEffectInstance.Dispose();
            item.Instances[i].SoundEffectInstance = null;
          }
        }
      }

      if (!item.Options.KeepSoundEffectInMemory)
      {
        item.SoundEffect.Dispose();
        item.SoundEffect = null;
      }
    }

    public void CancelAllSoundEffects()
    {
      foreach (var key in _soundEffectItems.Keys)
      {
        CancelSoundEffect(key);
      }
    }

    public bool IsSoundEffectPlaying(string name)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (_soundEffectItems.ContainsKey(name) == false)
        throw new ArgumentException(
          string.Format("No sound effect with key '{0}' registered.", name),
          "name");

      SoundEffectItem item = _soundEffectItems[name];

      for (int i = 0; i < _options.MaximumSoundEffectInstances; i++) {
        if (item.Instances[i] == null) continue;
        if (item.Instances[i].SoundEffectInstance == null) continue;
        if (item.Instances[i].SoundEffectInstance.State == SoundState.Playing)
        {
          return true;
        }
      }
      return false;
    }

    public void PlayMusic(string name)
    {
      PlayMusic(name, null);    
    }

    public void PlayMusic(string name, Action playingFinishedCallback)
    {
      if (name == null) throw new ArgumentNullException("name");
      if (_musicItems.ContainsKey(name) == false) 
        throw new ArgumentException(
          string.Format("No music with key '{0}' registered.", name), 
          "name");

      MusicItem item = _musicItems[name];

      if (MediaPlayer.State == MediaState.Playing || 
          MediaPlayer.State == MediaState.Paused)
      {
        MediaPlayer.Stop();
        //_currentMusic.Dispose();
      }

      _currentMusic = _contentManager.Load<Song>(item.AssetName);

      if (item.Options != null)
      {
        MediaPlayer.IsRepeating = item.Options.LoopMusic;
        _nextMusicName = item.Options.MusicToSwitchToWhenPlayingComplete;
      }
      else
      {
        MediaPlayer.IsRepeating = false;
        _nextMusicName = null;
      }

      MediaPlayer.Play(_currentMusic);

      SignalScriptsWaitingForMusicToComplete();

      _callBackWhenMusicFinished = playingFinishedCallback;
    }



    public void CueNextMusic(string name) {
      if (name == null) throw new ArgumentNullException("name");
      if (_musicItems.ContainsKey(name) == false)
        throw new ArgumentException(
          string.Format("No music with key '{0}' registered.", name),
          "name");

      _nextMusicName = name;
    }

    public void PauseMusic()
    {
      if (MediaPlayer.State == MediaState.Playing)
      {
        MediaPlayer.Pause();        
      }
    }

    public void ResumeMusic()
    {
      if (MediaPlayer.State == MediaState.Paused)
      {
        MediaPlayer.Resume();
      }
    }

    public bool IsPlayingMusic()
    {
      return MediaPlayer.State == MediaState.Playing;
    }

    public void Update(GameTime gameTime)
    {
      UpdateMusic(gameTime);
      UpdateSoundEffects(gameTime);
    }

    protected void UpdateMusic(GameTime gameTime)
    {
      if (MediaPlayer.State != MediaState.Stopped) return;

      if (_callBackWhenMusicFinished != null) {
        Action callback = _callBackWhenMusicFinished;
        _callBackWhenMusicFinished = null;
        callback();
      }

      SignalScriptsWaitingForMusicToComplete();

      if (_nextMusicName != null)
      {
        PlayMusic(_nextMusicName);
      }  
    }

    protected void UpdateSoundEffects(GameTime gameTime)
    {
      for (int i = _soundEffectsPlaying.Count - 1; i >= 0; i--)
      {
        var instance = _soundEffectsPlaying[i];
        if (instance.CallbackWhenSoundEffectFinished == null)
        {
          _soundEffectsPlaying.RemoveAt(i);
          continue;
        }

        if (instance.SoundEffectInstance.State != SoundState.Stopped) continue;

        Action callback = instance.CallbackWhenSoundEffectFinished;
        _soundEffectsPlaying.RemoveAt(i);

        if (instance.ScriptWaitingForSoundEffectToFinish != null)
        {
          instance.ScriptWaitingForSoundEffectToFinish.IsComplete = true;
        }

        if (callback != null)
        {
          callback();          
        }
      }
    }


    private List<ScriptState> _scriptsWaitingForMusicToFinish = new List<ScriptState>();
    public ScriptState WaitForMusicToFinish()
    {
      var state = new ScriptState();
      if (!IsPlayingMusic())
      {
        state.IsComplete = true;
      }
      else
      {
        _scriptsWaitingForMusicToFinish.Add(state);
      }
      return state;
    }

    private void SignalScriptsWaitingForMusicToComplete()
    {
      if (_scriptsWaitingForMusicToFinish.Any())
      {
        foreach (var state in _scriptsWaitingForMusicToFinish)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForMusicToFinish = new List<ScriptState>();
      }
    }
  }
}
