using System;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework.Audio;

namespace Dalbinblue.BaseGameLibrary.Audio
{
  internal class PlayingSoundEffectInstance
  {
    public SoundEffectInstance SoundEffectInstance { get; set; }
    public Action CallbackWhenSoundEffectFinished { get; set; }
    public ScriptState ScriptWaitingForSoundEffectToFinish { get; set; }
  }
}