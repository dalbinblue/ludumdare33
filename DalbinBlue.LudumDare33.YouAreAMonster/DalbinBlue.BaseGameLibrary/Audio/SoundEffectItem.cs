using Microsoft.Xna.Framework.Audio;

namespace Dalbinblue.BaseGameLibrary.Audio
{
  internal class SoundEffectItem
  {
    public string Name { get; set; }
    public string AssetName { get; set; }
    public SoundEffectOptions Options { get; set; }
    public SoundEffect SoundEffect { get; set; }
    public PlayingSoundEffectInstance[] Instances { get; set; }
  }
}