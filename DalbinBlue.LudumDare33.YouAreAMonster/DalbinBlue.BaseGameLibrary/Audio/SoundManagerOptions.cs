namespace Dalbinblue.BaseGameLibrary.Audio
{
  public class SoundManagerOptions
  {
    /// <summary>
    /// Specifies the maximum number of instances of a specific sound effect 
    /// that can be played at one time.  The higher the number, the larger the
    /// memory footprint the sound manager may take.
    /// </summary>
    public int MaximumSoundEffectInstances { get; set; }

    public static readonly SoundManagerOptions DefaultSoundManagerOptions =
      new SoundManagerOptions 
      {
        MaximumSoundEffectInstances = 4
      };
  }
}