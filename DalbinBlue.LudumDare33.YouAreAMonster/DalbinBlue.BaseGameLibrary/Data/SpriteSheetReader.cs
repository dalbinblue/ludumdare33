﻿using System.IO;
using System.Runtime.Serialization;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Microsoft.Xna.Framework.Content;

namespace Dalbinblue.BaseGameLibrary.Data {
  public class SpriteSheetReader : ContentTypeReader<SpriteSheet> {
    protected override SpriteSheet Read(ContentReader input, SpriteSheet existingInstance) {
      int dataLength = input.ReadInt32();
      byte[] data = input.ReadBytes(dataLength);
      using (var stream = new MemoryStream(data))
      {

        var serializer = new DataContractSerializer(typeof (SpriteSheet));
        var spriteSheet = (SpriteSheet)serializer.ReadObject(stream);

        //IFormatter binaryFormatter = new BinaryFormatter();
        //var spriteSheet = (SpriteSheet) binaryFormatter.Deserialize(stream);

        return spriteSheet;
      }
    }
  }
}
