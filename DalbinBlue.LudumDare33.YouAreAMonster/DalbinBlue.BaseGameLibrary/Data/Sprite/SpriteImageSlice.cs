using System;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Data.Sprite {
  [Serializable]
  public class SpriteImageSlice {
    public Rectangle ImageBounds { get; set; }
    public Rectangle CollisionBounds { get; set; }
    public Point Origin { get; set; }
  }
}