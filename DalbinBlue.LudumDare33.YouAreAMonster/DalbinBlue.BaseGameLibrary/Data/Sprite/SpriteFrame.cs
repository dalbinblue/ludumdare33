using System;

namespace Dalbinblue.BaseGameLibrary.Data.Sprite {
  [Serializable]
  public class SpriteFrame {
    public int Slice { get; set; }
    public float FrameLength { get; set; }
  }
}