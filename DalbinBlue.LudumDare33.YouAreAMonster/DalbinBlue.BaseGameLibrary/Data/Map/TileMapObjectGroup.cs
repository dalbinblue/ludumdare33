using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileMapObjectGroup {
    public string Name { get; set; }
    public Dictionary<string, string> Properties { get; set; }
    public TileMapObject[] Objects { get; set; }
  }
}