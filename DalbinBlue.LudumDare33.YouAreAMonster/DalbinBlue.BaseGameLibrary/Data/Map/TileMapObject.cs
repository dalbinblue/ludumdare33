using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileMapObject {
    public string Name { get; set; }
    public string Type { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public UInt32 TileId { get; set; }
    public bool IsVisible { get; set; }
    public Dictionary<string, string> Properties { get; set; }
    public TileMapObjectGeometryType GeometryType { get; set; }
    public Point[] GeometryPoints { get; set; }
  }
}