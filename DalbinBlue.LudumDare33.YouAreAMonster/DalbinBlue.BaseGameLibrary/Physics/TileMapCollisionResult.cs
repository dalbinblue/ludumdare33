using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Physics {
  public class TileMapCollisionResult {
    public bool HasCollided { get; set; }
    public List<Point> TileCollisionIndices { get; private set; }
    public Point UncollidingMovementOffset { get; set; }
    public Point OriginalOffset { get; set; }

    public bool CollisionOnBottom { get; set; }
    public bool CollisionOnTop { get; set; }
    public bool CollisionOnLeft { get; set; }
    public bool CollisionOnRight { get; set; }
    public bool OnPlatform { get; set; }

    public TileMapCollisionResult() {
      TileCollisionIndices = new List<Point>();
    }
  }
}