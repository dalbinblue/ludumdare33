﻿namespace Dalbinblue.BaseGameLibrary.Physics {
  internal enum TileCollisionEdgeType {
    Empty,
    Solid,
    Complex
  }
}