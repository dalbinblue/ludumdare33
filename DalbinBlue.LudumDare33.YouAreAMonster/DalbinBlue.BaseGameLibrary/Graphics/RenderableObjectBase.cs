using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public abstract class RenderableObjectBase : IRenderableObject {
    private readonly EasingFunc _defaultEasingFunction = Easing.LinearEasing;

    private Vector2 _position;
    private float _rotation;
    private Vector2 _scale;
    private Vector2 _origin;
    private Color _color;

    private bool _isRotationAnimating;
    private float _originalRotation;
    private float _targetRotation;
    private float _rotationAnimationDuration;
    private float _rotationCurrentTime;
    private EasingFunc _rotationEasingFunction;
    private string _rotationAnimationName;

    private bool _isScaleAnimating;
    private Vector2 _originalScale;
    private Vector2 _targetScale;
    private float _scaleAnimationDuration;
    private float _scaleCurrentTime;
    private EasingFunc _scaleEasingFunction;
    private string _scaleAnimationName;

    private bool _isOriginAnimating;
    private Vector2 _originalOrigin;
    private Vector2 _targetOrigin;
    private float _originAnimationDuration;
    private float _originCurrentTime;
    private EasingFunc _originEasingFunction;
    private string _originAnimationName;

    private bool _isPositionAnimating;
    private Vector2 _originalPosition;
    private Vector2 _targetPosition;
    private float _positionAnimationDuration;
    private float _positionCurrentTime;
    private EasingFunc _positionEasingFunction;
    private string _positionAnimationName;

    private bool _isColorAnimating;
    private Color _originalColor;
    private Color _targetColor;
    private float _colorAnimationDuration;
    private float _colorCurrentTime;
    private EasingFunc _colorEasingFunction;
    private string _colorAnimationName;



    public virtual Vector2 Position {
      get { return _position; }
      set {
        CancelPositionAnimation();
        _position = value; }
    }

    public virtual float Rotation {
      get { return _rotation; }
      set {
        CancelRotationAnimation();
        _rotation = value; }
    }

    public virtual Vector2 Scale {
      get { return _scale; }
      set {
        CancelScaleAnimation();
        _scale = value;
      }
    }

    public virtual Vector2 Origin {
      get { return _origin; }
      set {
        CancelOriginAnimation();
        _origin = value; }
    }

    public virtual Color Color {
      get { return _color; }
      set {
        CancelColorAnimation();
        _color = value;
      }
    }

    public int Width { get; protected set; }
    public int Height { get; protected set; }
    public bool RenderIfOutOfBounds { get; protected set; }
    public float Layer { get; set; }

    public event TweeningAnimationEventHandler TweeningAnimationComplete;

    protected virtual void OnTweeningAnimationComplete(TweeningAnimationEventHandlerArgs args) {
      TweeningAnimationEventHandler handler = TweeningAnimationComplete;
      if (handler != null) handler(this, args);
    }


    protected RenderableObjectBase() {
      _position = Vector2.Zero;
      _rotation = 0.0f;
      _scale = Vector2.One;
      _origin = Vector2.Zero;
      _color = Color.White;
      Width = 0;
      Height = 0;
      Layer = 0.0f;
      RenderIfOutOfBounds = true;
    }

    public virtual Rectangle GetBounds() {
      int x = (int)(Position.X - Origin.X);
      int y = (int)(Position.Y - Origin.Y);
      return new Rectangle(x, y, Width, Height);
    }

    // Parent child transformation matrix code based on:
    // http://gamedev.stackexchange.com/questions/23034/is-there-a-simple-way-to-group-two-or-more-sprites-so-all-of-them-will-be-depen

    protected virtual Matrix GetLocalTransformationMatrix() {
      return Matrix.CreateTranslation(-Origin.X, -Origin.Y, 0f)
             *Matrix.CreateScale(Scale.X, Scale.Y, 1f)
             *Matrix.CreateRotationZ(Rotation)
             *Matrix.CreateTranslation(Position.X, Position.Y, 0f);
    }

    protected TransformationData DecomposeTransformationMatrix(ref Matrix transformationMatrix) {
      Vector3 translation3;
      Vector3 scale3;
      Quaternion rotation4;
      transformationMatrix.Decompose(out scale3, out rotation4, out translation3);
      Vector2 direction = Vector2.Transform(Vector2.UnitX, rotation4);

      var scale = new Vector2(scale3.X, scale3.Y);
      var rotation = (float) Math.Atan2(direction.Y, direction.X);

      return new TransformationData
      {
        Rotation = TransformRotation(scale, rotation),
        Position = new Vector2(translation3.X, translation3.Y),
        Scale = scale
      };
    }

    private float TransformRotation(Vector2 scale, float rotation) {
      if ((scale.X < 0) ^ (scale.Y < 0)) {
        rotation = -rotation;
      }
      return rotation;
    }
    // End of transformation matrix code




    public void AnimateRotationBySpeed(float targetRotation, float speed) {
      AnimateRotationBySpeed(targetRotation, speed, _defaultEasingFunction, null);
    }

    public void AnimateRotationBySpeed(float targetRotation, float speed, EasingFunc easing) {
      AnimateRotationBySpeed(targetRotation, speed, easing, null);
    }

    public void AnimateRotationBySpeed(float targetRotation, float speed, EasingFunc easing, string animationName) {
      float rotationDistance = GetMinimalRotationAmount(Rotation, targetRotation);
      AnimateRotation(targetRotation, rotationDistance / speed, easing, animationName);
    }

    public void AnimateRotation(float targetRotation, float duration) {
      AnimateRotation(targetRotation, duration, _defaultEasingFunction, null);
    }

    public void AnimateRotation(float targetRotation, float duration, EasingFunc easing) {
      AnimateRotation(targetRotation, duration, easing, null);
    }

    public void AnimateRotation(float targetRotation, float duration, EasingFunc easing, string animationName) {
      _originalRotation = GetMinimalOriginalRotationToTarget(Rotation, targetRotation);
      _targetRotation = targetRotation;
      _rotationAnimationDuration = duration;
      _rotationCurrentTime = 0;
      _isRotationAnimating = true;
      _rotationEasingFunction = easing;
      _rotationAnimationName = animationName;
      SignalScriptsWaitingForRotationToComplete();
    }

    private float GetMinimalOriginalRotationToTarget(float originalRotation, float targetRotation) {
      float currentDistance = Math.Abs(targetRotation - originalRotation);
      float rotatedDownDistance = Math.Abs(targetRotation - (originalRotation - MathHelper.TwoPi));
      float rotatedUpDistance = Math.Abs(targetRotation - (originalRotation + MathHelper.TwoPi));

      if (currentDistance < rotatedDownDistance)
      {
        if (currentDistance < rotatedUpDistance)
        {
          return originalRotation;
        }
        return originalRotation + MathHelper.TwoPi;
      }
      if (rotatedDownDistance < rotatedUpDistance)
      {
        return originalRotation - MathHelper.TwoPi;
      }
      return originalRotation + MathHelper.TwoPi;
    }

    private float GetMinimalRotationAmount(float originalRotation, float targetRotation) {
      return Math.Abs(targetRotation - GetMinimalOriginalRotationToTarget(originalRotation, targetRotation));
    }



    public void AnimateScaleBySpeed(Vector2 targetScale, float speed) {
      AnimateScaleBySpeed(targetScale, speed, _defaultEasingFunction, null);
    }

    public void AnimateScaleBySpeed(Vector2 targetScale, float speed, EasingFunc easing) {
      AnimateScaleBySpeed(targetScale, speed, easing, null);
    }

    public void AnimateScaleBySpeed(Vector2 targetScale, float speed, EasingFunc easing, string animationName) {
      float horizontalChange = Math.Abs(targetScale.X - _scale.X);
      float verticalDistance = Math.Abs(targetScale.Y - _scale.Y);
      var maxChange = Math.Max(horizontalChange, verticalDistance);
      AnimateScale(targetScale, maxChange / speed, easing, animationName);
    }

    public void AnimateScale(Vector2 targeScale, float duration) {
      AnimateScale(targeScale, duration, _defaultEasingFunction, null);
    }

    public void AnimateScale(Vector2 targetScale, float duration, EasingFunc easing) {
      AnimateScale(targetScale, duration, easing, null);
    }

    public void AnimateScale(Vector2 targetScale, float duration, EasingFunc easing, string animationName) {
      _isScaleAnimating = true;
      _originalScale = _scale;
      _targetScale = targetScale;
      _scaleAnimationDuration = duration;
      _scaleCurrentTime = 0.0f;
      _scaleEasingFunction = easing;
      _scaleAnimationName = animationName;
      SignalScriptsWaitingForScaleToComplete();
    }



    public void AnimateOrigin(Vector2 offset, float duration) {
      AnimateOrigin(offset, duration, _defaultEasingFunction, null);
    }

    public void AnimateOrigin(Vector2 offset, float duration, EasingFunc easing) {
      AnimateOrigin(offset, duration, easing, null);
    }

    public void AnimateOrigin(Vector2 offset, float duration, EasingFunc easing, string animationName) {
      _isOriginAnimating = true;
      _originalOrigin = _targetOrigin;
      _targetOrigin = offset;
      _originAnimationDuration = duration;
      _originCurrentTime = 0.0f;
      _originEasingFunction = easing;
      _originAnimationName = animationName;
      SignalScriptsWaitingForOriginToComplete();
    }

    public void AnimateOriginBySpeed(Vector2 offset, float speed) {
      AnimateOriginBySpeed(offset, speed, _defaultEasingFunction, null);
    }

    public void AnimateOriginBySpeed(Vector2 offset, float speed, EasingFunc easing) {
      AnimateOriginBySpeed(offset, speed, easing, null);
    }

    public void AnimateOriginBySpeed(Vector2 offset, float speed, EasingFunc easing, string animationName) {
      float horizontalDistance = Math.Abs(offset.X - _targetOrigin.X);
      float verticalDistance = Math.Abs(offset.Y - _targetOrigin.Y);
      var maxDistance = Math.Max(horizontalDistance, verticalDistance);
      AnimateOrigin(offset, maxDistance / speed, easing, animationName);
    }



    public void AnimatePosition(Vector2 position, float duration) {
      AnimatePosition(position, duration, _defaultEasingFunction, null);
    }

    public void AnimatePosition(Vector2 position, float duration, EasingFunc easing) {
      AnimatePosition(position, duration, easing, null);
    }

    public void AnimatePosition(Vector2 position, float duration, EasingFunc easing, string animationName) {
      _isPositionAnimating = true;
      _originalPosition = _position;
      _targetPosition = position;
      _positionAnimationDuration = duration;
      _positionCurrentTime = 0.0f;
      _positionEasingFunction = easing;
      _positionAnimationName = animationName;
      SignalScriptsWaitingForPositionToComplete();
    }

    public void AnimatePositionBySpeed(Vector2 position, float speed)
    {
      AnimatePositionBySpeed(position, speed, _defaultEasingFunction, null);
    }

    public void AnimatePositionBySpeed(Vector2 position, float speed, EasingFunc easing)
    {
      AnimatePositionBySpeed(position, speed, easing, null);
    }

    public void AnimatePositionBySpeed(Vector2 position, float speed, EasingFunc easing, string animationName)
    {
      float horizontalDistance = Math.Abs(position.X - _targetPosition.X);
      float verticalDistance = Math.Abs(position.Y - _targetPosition.Y);
      var maxDistance = Math.Max(horizontalDistance, verticalDistance);
      AnimatePosition(position, maxDistance / speed, easing, animationName);
    }



    public void AnimateColor(Color color, float duration)
    {
      AnimateColor(color, duration, _defaultEasingFunction, null);
    }

    public void AnimateColor(Color color, float duration, EasingFunc easing)
    {
      AnimateColor(color, duration, easing, null);
    }

    public void AnimateColor(Color color, float duration, EasingFunc easing, string animationName)
    {
      _isColorAnimating = true;
      _originalColor = _color;
      _targetColor = color;
      _colorAnimationDuration = duration;
      _colorCurrentTime = 0.0f;
      _colorEasingFunction = easing;
      _colorAnimationName = animationName;
      SignalScriptsWaitingForColorToComplete();
    }

    public void AnimateColorBySpeed(Color color, float speed)
    {
      AnimateColorBySpeed(color, speed, _defaultEasingFunction, null);
    }

    public void AnimateColorBySpeed(Color color, float speed, EasingFunc easing)
    {
      AnimateColorBySpeed(color, speed, easing, null);
    }

    public void AnimateColorBySpeed(Color color, float speed, EasingFunc easing, string animationName)
    {
      int rDistance = Math.Abs(color.R - _targetColor.R);
      int gDistance = Math.Abs(color.G - _targetColor.G);
      int bDistance = Math.Abs(color.B - _targetColor.B);
      int aDistance = Math.Abs(color.A - _targetColor.A);

      var maxDistance = Math.Max(rDistance, Math.Max(gDistance, Math.Max(bDistance, aDistance)));
      AnimateColor(color, maxDistance / speed, easing, animationName);
    }



    protected void CancelPositionAnimation() {
      _isPositionAnimating = false;
      _originalPosition = _position;
      _targetPosition = _position;
      _positionAnimationDuration = 0.0f;
      _positionCurrentTime = 0.0f;
      _positionEasingFunction = null;
      SignalScriptsWaitingForPositionToComplete();
    }

    protected void CancelRotationAnimation() {
      _isRotationAnimating = false;
      _originalRotation = _rotation;
      _targetRotation = _rotation;
      _rotationAnimationDuration = 0.0f;
      _rotationCurrentTime = 0.0f;
      _rotationEasingFunction = null;
      SignalScriptsWaitingForRotationToComplete();
    }

    protected void CancelScaleAnimation() {
      _isScaleAnimating = false;
      _originalScale = _scale;
      _targetScale = _scale;
      _scaleAnimationDuration = 0.0f;
      _scaleCurrentTime = 0.0f;
      _scaleEasingFunction = null;
      SignalScriptsWaitingForScaleToComplete();
    }

    protected void CancelOriginAnimation() {
      _isOriginAnimating = false;
      _originalOrigin = _origin;
      _targetOrigin = _origin;
      _originAnimationDuration = 0.0f;
      _originCurrentTime = 0.0f;
      _originEasingFunction = null;
      SignalScriptsWaitingForOriginToComplete();
    }

    protected void CancelColorAnimation()
    {
      _isColorAnimating = false;
      _originalColor = _color;
      _targetColor = _color;
      _colorAnimationDuration = 0.0f;
      _colorCurrentTime = 0.0f;
      _colorEasingFunction = null;
      SignalScriptsWaitingForColorToComplete();
    }


    public void Update(GameTime gameTime) {
      PreMovementUpdate(gameTime);
      UpdateTweenAnimations(gameTime);
      PostMovementUpdate(gameTime);
    }

    private void UpdateTweenAnimations(GameTime gameTime) {
      var secondsElapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

      if (_isRotationAnimating) UpdateRotationAnimation(secondsElapsed);
      if (_isScaleAnimating) UpdateScaleAnimation(secondsElapsed);
      if (_isOriginAnimating) UpdateOriginAnimation(secondsElapsed);
      if (_isPositionAnimating) UpdatePositionAnimation(secondsElapsed);
      if (_isColorAnimating) UpdateColorAnimation(secondsElapsed);
    }

    private void UpdateRotationAnimation(float secondsElapsed) {
      _rotationCurrentTime += secondsElapsed;

      if (_rotationCurrentTime >= _rotationAnimationDuration) {
        _isRotationAnimating = false;
        _rotation = _targetRotation;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Rotation, AnimationName = _rotationAnimationName });
        SignalScriptsWaitingForRotationToComplete();
      }
      else {
        var percentAnimationComplete = _rotationCurrentTime / _rotationAnimationDuration;
        _rotation = _rotationEasingFunction(_originalRotation, _targetRotation, percentAnimationComplete);
      }
    }

    private void UpdateScaleAnimation(float secondsElapsed) {
      _scaleCurrentTime += secondsElapsed;

      if (_scaleCurrentTime >= _scaleAnimationDuration) {
        _isScaleAnimating = false;
        _scale = _targetScale;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Scale, AnimationName = _scaleAnimationName });
        SignalScriptsWaitingForScaleToComplete();
      }
      else {
        var percentAnimationComplete = _scaleCurrentTime / _scaleAnimationDuration;
        var newX = _scaleEasingFunction(_originalScale.X,
                                        _targetScale.X,
                                        percentAnimationComplete);
        var newY = _scaleEasingFunction(_originalScale.Y,
                                        _targetScale.Y,
                                        percentAnimationComplete);
        _scale = new Vector2(newX,newY);
      }
    }

    private void UpdateOriginAnimation(float secondsElapsed) {
      _originCurrentTime += secondsElapsed;

      if (_originCurrentTime > _originAnimationDuration) {
        _isOriginAnimating = false;
        _origin = _targetOrigin;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Origin, AnimationName = _originAnimationName });
        SignalScriptsWaitingForOriginToComplete();
      }
      else {
        var percentAnimationComplete = _originCurrentTime / _originAnimationDuration;
        var newX = _originEasingFunction(_originalOrigin.X,
                                         _targetOrigin.X,
                                         percentAnimationComplete);
        var newY = _originEasingFunction(_originalOrigin.Y,
                                         _targetOrigin.Y,
                                         percentAnimationComplete);
        _origin = new Vector2(newX, newY);
      }
    }

    private void UpdatePositionAnimation(float secondsElapsed) {
      _positionCurrentTime += secondsElapsed;

      if (_positionCurrentTime > _positionAnimationDuration) {
        _isPositionAnimating = false;
        _position = _targetPosition;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Position, AnimationName = _positionAnimationName });
        SignalScriptsWaitingForPositionToComplete();
      }
      else {
        var percentAnimationComplete = _positionCurrentTime / _positionAnimationDuration;
        var newX = _positionEasingFunction(_originalPosition.X,
                                         _targetPosition.X,
                                         percentAnimationComplete);
        var newY = _positionEasingFunction(_originalPosition.Y,
                                         _targetPosition.Y,
                                         percentAnimationComplete);
        _position = new Vector2(newX, newY);
      }
    }

    private void UpdateColorAnimation(float secondsElapsed)
    {
      _colorCurrentTime += secondsElapsed;

      if (_colorCurrentTime > _colorAnimationDuration)
      {
        _isColorAnimating = false;
        _color = _targetColor;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Color, AnimationName = _colorAnimationName });
        SignalScriptsWaitingForColorToComplete();
      }
      else
      {
        var percentAnimationComplete = _colorCurrentTime / _colorAnimationDuration;

        var newR = (int)_colorEasingFunction(_originalColor.R, _targetColor.R, percentAnimationComplete);
        var newG = (int)_colorEasingFunction(_originalColor.G, _targetColor.G, percentAnimationComplete);
        var newB = (int)_colorEasingFunction(_originalColor.B, _targetColor.B, percentAnimationComplete);
        var newA = (int)_colorEasingFunction(_originalColor.A, _targetColor.A, percentAnimationComplete);
        _color = new Color(newR, newG, newB, newA);
      }
    }


    public virtual void PreMovementUpdate(GameTime gameTime) {}
    public virtual void PostMovementUpdate(GameTime gameTime) {}
  
    public abstract void Draw(SpriteBatch spriteBatch, Matrix parentTransformation);


    private List<ScriptState> _scriptsWaitingForRotationToComplete = new List<ScriptState>();
    public ScriptState WaitForRotationAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isRotationAnimating)
      {
        _scriptsWaitingForRotationToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForRotationToComplete()
    {
      if (_scriptsWaitingForRotationToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForRotationToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForRotationToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForScaleToComplete = new List<ScriptState>();
    public ScriptState WaitForScaleAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isScaleAnimating)
      {
        _scriptsWaitingForScaleToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForScaleToComplete()
    {
      if (_scriptsWaitingForScaleToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForScaleToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForScaleToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForOriginToComplete = new List<ScriptState>();
    public ScriptState WaitForOriginAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isOriginAnimating)
      {
        _scriptsWaitingForOriginToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForOriginToComplete()
    {
      if (_scriptsWaitingForOriginToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForOriginToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForOriginToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForPositionToComplete = new List<ScriptState>();
    public ScriptState WaitForPositionAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isPositionAnimating)
      {
        _scriptsWaitingForPositionToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForPositionToComplete()
    {
      if (_scriptsWaitingForPositionToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForPositionToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForPositionToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForColorToComplete = new List<ScriptState>();
    public ScriptState WaitForColorAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isColorAnimating)
      {
        _scriptsWaitingForColorToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForColorToComplete()
    {
      if (_scriptsWaitingForColorToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForColorToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForColorToComplete = new List<ScriptState>();
      }
    }
  }
}