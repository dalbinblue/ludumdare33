﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class StaticImage : RenderableObjectBase
  {
    private Texture2D _texture;
    public Rectangle TextureCoordinates;
    public int RepeatX { get; set; }
    public int RepeatY { get; set; }


    public StaticImage(Texture2D texture)
      : this(texture, texture.Width, texture.Height, new Rectangle(0, 0, texture.Width, texture.Height))
    {
      // DO NOTHING
    }

    public StaticImage(Texture2D texture, int width, int height)
      : this(texture,width,height,new Rectangle(0,0,texture.Width,texture.Height))
    {
      // DO NOTHING
    }

    public StaticImage(Texture2D texture, Rectangle textureCoordinates)
      : this(texture,textureCoordinates.Width,textureCoordinates.Height,textureCoordinates)
    {
      // DO NOTHING
    }

    public StaticImage(Texture2D texture, int width, int height, Rectangle textureCoordinates)
    {
      RepeatX = 1;
      RepeatY = 1;
      _texture = texture;
      Width = width;
      Height = height;
      TextureCoordinates = textureCoordinates;
    }

    public override void PostMovementUpdate(GameTime gameTime)
    {
      //DO NOTHING
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {

      for (int x = 0; x < RepeatX; x++)
      {
        for (int y = 0; y < RepeatY; y++)
        {
          var transformationMatrix = Matrix.CreateTranslation(x * Width, y * Height, 0f) * GetLocalTransformationMatrix() * parentTransformation;
          TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

          spriteBatch.Draw(_texture
                           , transformationData.Position
                           , TextureCoordinates
                           , Color
                           , transformationData.Rotation
                           , Origin
                           , transformationData.Scale
                           , SpriteEffects.None
                           , Layer);
        }
      }
    }
  }
}
