﻿using System;
using System.Collections.Generic;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;

namespace Dalbinblue.BaseGameLibrary.Graphics
{
  public class LimitingTileGrid : RenderableObjectBase
  {

    public int TileWidth { get; protected set; }
    public int TileHeight { get; protected set; }
    public int WidthInTiles { get; protected set; }
    public int HeightInTiles { get; protected set; }

    private TileGridTile[][] _gridTiles;

    public static LimitingTileGrid FromTileMapLayer(TileMap tileMap, int layerIndex, ContentManager content)
    {
      var layer = tileMap.Layers[layerIndex];
      var tileGrid = new LimitingTileGrid
      {
        TileWidth = tileMap.TileWidth,
        TileHeight = tileMap.TileHeight,
        WidthInTiles = tileMap.WidthInTiles,
        HeightInTiles = tileMap.HeightInTiles,
        Width = tileMap.TileWidth * tileMap.WidthInTiles,
        Height = tileMap.TileHeight * tileMap.HeightInTiles,
        Position = Vector2.Zero,
        Origin = Vector2.Zero,
        Layer = 0,
        RenderIfOutOfBounds = false
      };


      var tilesets = new FixedSizeTileset[tileMap.Tilesets.Count()];
      for (int i = 0; i < tileMap.Tilesets.Count(); i++)
      {
        tilesets[i] = FixedSizeTileset.FromTileMapTilesetDefinition(tileMap.Tilesets[i], content);
      }

      tileGrid._gridTiles = new TileGridTile[tileMap.WidthInTiles][];

      for (int x = 0; x < tileMap.WidthInTiles; x++)
      {
        tileGrid._gridTiles[x] = new TileGridTile[tileMap.HeightInTiles];

        for (int y = 0; y < tileMap.HeightInTiles; y++)
        {
          int tileId = Convert.ToInt32(layer.LayerTiles[x][y] & 0x0000FFFF);

          // If tileId = 0, then there is no tile at that position.  Let the default null be used.
          if (tileId == 0) continue;

          // Find the tileset for the tile.

          var foundTileSet = tileMap.Tilesets.Where(ts => ts.FirstTileId <= tileId).OrderByDescending(ts => ts.FirstTileId).First();

          int tileSetId = 0;

          for (int i = 0; i < tileMap.Tilesets.Count(); i++)
          {
            if (tileMap.Tilesets[i].FirstTileId == foundTileSet.FirstTileId)
            {
              tileSetId = i;
              break;
            }
          }

          // Get the tileId for the tile
          TileMapTilesetDefinition tilesetDefinition = tileMap.Tilesets[tileSetId];
          FixedSizeTileset tileset = tilesets[tileSetId];
          int normalizedTileId = tileId - tilesetDefinition.FirstTileId;

          tileGrid._gridTiles[x][y] = new TileGridTile { TileId = normalizedTileId, Tileset = tileset };
        }
      }

      return tileGrid;
    }

    public override void PostMovementUpdate(GameTime gameTime)
    {
      // DO NOTHING
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      Matrix worldMatrix = parentTransformation*RenderEngine.CurrentCameraTransformationMatrix;
      TransformationData worldTransformationData = DecomposeTransformationMatrix(ref worldMatrix);
      int minX = (int)(-worldTransformationData.Position.X / worldTransformationData.Scale.X) / TileWidth  - 4;
      int minY = (int)(-worldTransformationData.Position.Y / worldTransformationData.Scale.Y) / TileHeight - 4;
      int maxX = minX + (320 / TileWidth) + 8;
      int maxY = minY + (240 / TileHeight) + 8;

      if (minX < 0) minX = 0;
      if (minY < 0) minY = 0;
      if (maxX > WidthInTiles) maxX = WidthInTiles;
      if (maxY > HeightInTiles) maxY = HeightInTiles;

      for (int x = minX; x < maxX; x++) {
        for (int y = minY; y < maxY; y++) {
          var tile = _gridTiles[x][y];

          if (tile == null) continue;

          var transformationMatrix = Matrix.CreateTranslation(x * TileWidth, y * TileHeight, 0f) * GetLocalTransformationMatrix() * parentTransformation;
          TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

          spriteBatch.Draw(
            tile.Tileset.Texture,
            transformationData.Position,
            tile.Tileset.GetTileBounds(tile.TileId),
            Color,
            transformationData.Rotation,
            Origin,
            transformationData.Scale,
            SpriteEffects.None, 
            Layer);
        }
      }
    }

    /// <summary>
    /// Returns a list of the tile coordinates that are within the given bounds
    /// area.
    /// </summary>
    public List<Point> GetTilesWithinBoundsForCollisionCheck(Rectangle bounds)
    {
      Rectangle gridBounds = new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
      if (!bounds.Intersects(gridBounds)) return new List<Point>();

      int minPixelX = (int)(bounds.X - Position.X);
      int minPixelY = (int)(bounds.Y - Position.Y);

      int minTileX = minPixelX / TileWidth - 1;
      int minTileY = minPixelY / TileHeight - 1;

      int tileXSpan = Convert.ToInt32(Math.Ceiling((double)(bounds.Width + TileWidth - 1) / TileWidth)) + 2;
      int tileYSpan = Convert.ToInt32(Math.Ceiling((double)(bounds.Height + TileHeight - 1) / TileHeight)) + 2;

      if (minTileX < 0)
      {
        tileXSpan += minTileX;
        minTileX = 0;
      }

      if (minTileY < 0)
      {
        tileYSpan += minTileY;
        minTileY = 0;
      }

      if (minTileX + tileXSpan > WidthInTiles)
      {
        tileXSpan = WidthInTiles - minTileX;
      }

      if (minTileY + tileYSpan > HeightInTiles)
      {
        tileYSpan = HeightInTiles - minTileY;
      }

      var points = new List<Point>();

      for (var x = minTileX; x < minTileX + tileXSpan; x++)
      {
        for (var y = minTileY; y < minTileY + tileYSpan; y++)
        {
          points.Add(new Point(x, y));
        }
      }

      return points;
    }

    /// <summary>
    /// Get the collision bounds for a tile based on the current position of 
    /// the TileGrid and the specified tile on that Grid.
    /// </summary>
    public Rectangle GetTileBounds(Point tileCoordinate)
    {
      return new Rectangle(
        (int)Position.X + (tileCoordinate.X * TileWidth),
        (int)Position.Y + (tileCoordinate.Y * TileHeight),
        TileWidth,
        TileHeight);
    }
  }
}