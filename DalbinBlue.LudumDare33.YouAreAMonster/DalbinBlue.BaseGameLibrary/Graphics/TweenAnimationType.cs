namespace Dalbinblue.BaseGameLibrary.Graphics {
  public enum TweenAnimationType {
    Rotation,
    Position,
    Offset,
    Scale,
    Origin,
    Color
  }
}