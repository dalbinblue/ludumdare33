namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class TweeningAnimationEventHandlerArgs {
    public TweenAnimationType AnimationType { get; set; }
    public string AnimationName { get; set; }

  }
}