﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud {
  public class IntegerDisplay : RenderableObjectBase
  {
    private SpriteFont _font;
    public string Format { get; set; }
    public int Value { get; set; }

    public IntegerDisplay(SpriteFont font) 
      : this(font, "{0}")
    {
      // DO NOTHING
    }

    public IntegerDisplay(SpriteFont font, string format)
    {
      _font = font;
      Format = format;
    }

    public override void PostMovementUpdate(GameTime gameTime)
    {
      // DO NOTHING
    }


    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
      TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);
      spriteBatch.DrawString(
        _font, 
        string.Format(Format, Value), 
        transformationData.Position, 
        Color, 
        transformationData.Rotation, 
        Origin, 
        transformationData.Scale, 
        SpriteEffects.None, 
        Layer);
    }
  }
}
