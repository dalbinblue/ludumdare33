﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud {
  public class IconCounter : RenderableObjectBase {
    private SpriteFont _font;
    private Texture2D _texture;
    private Rectangle _textureCoordinates;
    public int Value { get; set; }
    public int MaxRepeatedIconsBeforeShowingNumbers { get; set; }
    
    public IconCounter(SpriteFont font, Texture2D texture, Rectangle textureCoordinates)
    {
      _font = font;
      _texture = texture;
      _textureCoordinates = textureCoordinates;
    }


    public override void PostMovementUpdate(GameTime gameTime)
    {
      //DO NOTHING
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      //var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
      //TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      if (Value <= MaxRepeatedIconsBeforeShowingNumbers)
      {
        for (int i = 0; i < Value; i++) {
          var transformationMatrix = Matrix.CreateTranslation(i * _textureCoordinates.Width, 0f, 0f) * GetLocalTransformationMatrix() * parentTransformation;
          TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);
          spriteBatch.Draw(_texture, transformationData.Position, _textureCoordinates, Color, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);
        }
      }
      else
      {
        var iconTransformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
        TransformationData iconTransformationData = DecomposeTransformationMatrix(ref iconTransformationMatrix);
        spriteBatch.Draw(_texture, iconTransformationData.Position, _textureCoordinates, Color, iconTransformationData.Rotation, Origin, iconTransformationData.Scale, SpriteEffects.None, Layer);

        var textTransformationMatrix = Matrix.CreateTranslation(_textureCoordinates.Width, 0f, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        TransformationData textTransformationData = DecomposeTransformationMatrix(ref textTransformationMatrix);        
        spriteBatch.DrawString(_font, string.Format("X{0}", Value), textTransformationData.Position, Color, textTransformationData.Rotation, Origin, textTransformationData.Scale, SpriteEffects.None, Layer);
      }
    }
  }
}
