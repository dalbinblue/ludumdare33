﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud
{
  public class BorderBox : RenderableObjectBase {
    private Texture2D _texture;
    private readonly Rectangle _textureCoordinates;
    private int _borderLeft;
    private int _borderRight;
    private int _borderTop;
    private int _borderBottom;

    public BorderBox(Texture2D texture, Rectangle textureCoordinates, int width, int height, int borderWidth) 
    :this(texture,textureCoordinates,width,height,borderWidth,borderWidth,borderWidth,borderWidth)
    {
      // DO NOTHING
    }

    public BorderBox(Texture2D texture, Rectangle textureCoordinates, int width, int height, int borderLeft, int borderRight, int borderTop, int borderBottom) {
      Width = width;
      Height = height;
      _texture = texture;
      _textureCoordinates = textureCoordinates;
      _borderLeft = borderLeft;
      _borderRight = borderRight;
      _borderTop = borderTop;
      _borderBottom = borderBottom;
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      // Top-left corner
      var transformationMatrix = GetLocalTransformationMatrix()*parentTransformation;
      TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left,_textureCoordinates.Top, _borderLeft, _borderTop)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Top-right corner
      transformationMatrix = Matrix.CreateTranslation(Width - _borderRight, 0f, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _textureCoordinates.Width - _borderRight, _textureCoordinates.Top, _borderRight, _borderTop)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);


      // Bottom-left corner
      transformationMatrix = Matrix.CreateTranslation(0f, Height - _borderBottom, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left, _textureCoordinates.Top + _textureCoordinates.Height - _borderBottom, _borderLeft, _borderBottom)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);
      
      // Bottom-right corner
      transformationMatrix = Matrix.CreateTranslation(Width - _borderRight, Height - _borderBottom, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _textureCoordinates.Width - _borderRight, _textureCoordinates.Top + _textureCoordinates.Height - _borderBottom, _borderRight, _borderBottom)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Top Edge
      int edgeWidth = Width - _borderLeft - _borderRight;
      int textureEdgeWidth = _textureCoordinates.Width - _borderLeft - _borderRight;

      transformationMatrix = Matrix.CreateScale(edgeWidth / (float)textureEdgeWidth, 1f, 1f) * Matrix.CreateTranslation(_borderLeft, 0f, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _borderLeft, _textureCoordinates.Top, textureEdgeWidth, _borderTop)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Bottom Edge
      transformationMatrix = Matrix.CreateScale(edgeWidth / (float)textureEdgeWidth, 1f, 1f) * Matrix.CreateTranslation(_borderLeft, Height - _borderBottom, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _borderLeft, _textureCoordinates.Top + _textureCoordinates.Height - _borderBottom, textureEdgeWidth, _borderBottom)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Left Edge
      int edgeHeight = Height - _borderTop - _borderBottom;
      int textureEdgeHeight = _textureCoordinates.Height - _borderTop - _borderBottom;

      transformationMatrix = Matrix.CreateScale(1f, edgeHeight / (float)textureEdgeHeight, 1f) * Matrix.CreateTranslation(0f, _borderTop, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left, _textureCoordinates.Top + _borderTop, _borderLeft, textureEdgeHeight)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Right Edge
      transformationMatrix = Matrix.CreateScale(1f, edgeHeight / (float)textureEdgeHeight, 1f) * Matrix.CreateTranslation(Width - _borderRight, _borderTop, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _textureCoordinates.Width - _borderRight, _textureCoordinates.Top + _borderTop, _borderRight, textureEdgeHeight)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

      // Fill
      transformationMatrix = Matrix.CreateScale(edgeWidth / (float)textureEdgeWidth, edgeHeight / (float)textureEdgeHeight, 1f) * Matrix.CreateTranslation(_borderLeft, _borderTop, 0f) * GetLocalTransformationMatrix() * parentTransformation;
      transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      spriteBatch.Draw(_texture
                 , transformationData.Position
                 , new Rectangle(_textureCoordinates.Left + _borderLeft, _textureCoordinates.Top + _borderTop, textureEdgeWidth, textureEdgeHeight)
                 , Color
                 , transformationData.Rotation
                 , Origin
                 , transformationData.Scale
                 , SpriteEffects.None
                 , Layer);

    }
  }
}
