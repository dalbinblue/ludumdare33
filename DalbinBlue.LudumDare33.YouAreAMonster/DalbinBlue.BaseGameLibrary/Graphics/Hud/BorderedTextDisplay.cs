﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud {
  public class BorderedTextDisplay : RenderableObjectBase
  {
    private readonly SpriteFont _font;
    public string Text { get; set; }
    public int MaximumWidth { get; set; }
    public int LineHeight { get; set; }
    public Color BorderColor { get; set; }

    public float FullTextWidth {
      get { return _font.MeasureString(Text).X; }
    }

    public BorderedTextDisplay(SpriteFont font, int maximumWidth)
    {
      MaximumWidth = maximumWidth;
      _font = font;
      LineHeight = _font.LineSpacing;
      Text = string.Empty;
      BorderColor = Color.Black;
    }

    public override void PostMovementUpdate(GameTime gameTime)
    {
      // DO NOTHING
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation)
    {
      IEnumerable<string> lines = SplitStringIntoLines(Text);
      int lineNumber = 0;
      foreach (var line in lines)
      {
        var transformationMatrix = Matrix.CreateTranslation(0f + 1, lineNumber * LineHeight, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, BorderColor, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);

        transformationMatrix = Matrix.CreateTranslation(0f - 1, lineNumber * LineHeight, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, BorderColor, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);

        transformationMatrix = Matrix.CreateTranslation(0f, lineNumber * LineHeight -1, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, BorderColor, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);

        transformationMatrix = Matrix.CreateTranslation(0f, lineNumber * LineHeight + 1, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, BorderColor, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);        

        transformationMatrix = Matrix.CreateTranslation(0f, lineNumber * LineHeight, 0f) * GetLocalTransformationMatrix() * parentTransformation;
        transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, Color, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);
        
        lineNumber += 1;
      }
    }

    private IEnumerable<string> SplitStringIntoLines(string text)
    {
      if (string.IsNullOrEmpty(text)) return new List<string>();

      var words = text.Split(' ');

      string currentLine = "";
      var lines = new List<string>();

      foreach (var word in words)
      {
        string newLine;

        if (currentLine == "")
        {
          newLine = word;
        }
        else
        {
          newLine = currentLine + " " + word;
        }

        if (_font.MeasureString(newLine).X > MaximumWidth)
        {
          lines.Add(currentLine);
          currentLine = word;
        }
        else
        {
          currentLine = newLine;
        }
      }

      if (currentLine != string.Empty)
      {
        lines.Add(currentLine);
      }

      return lines.ToArray();
    }

    public int GetLineCount()
    {
      return SplitStringIntoLines(Text).Count();
    }
  }
}