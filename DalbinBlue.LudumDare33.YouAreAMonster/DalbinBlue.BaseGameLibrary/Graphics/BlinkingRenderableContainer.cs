﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics
{
  public class BlinkingRenderableContainer : RenderableObjectBase {

    private IRenderableObject _child;
    private double _blinkOnPeriod;
    private double _blinkOffPeriod;
    private double _timeInCurrentState = 0f;
    private bool _isDrawing = true;

    public BlinkingRenderableContainer(IRenderableObject child, double blinkTime) {
      _child = child;
      _blinkOffPeriod = blinkTime/2f;
      _blinkOnPeriod = blinkTime/2f;
    }

    public BlinkingRenderableContainer(IRenderableObject child, double blinkOnPeriod, double blinkOffPeriod) {
      _child = child;
      _blinkOnPeriod = blinkOnPeriod;
      _blinkOffPeriod = blinkOffPeriod;
    }

    public override void PreMovementUpdate(GameTime gameTime)
    {
      base.PreMovementUpdate(gameTime);
      _child.Update(gameTime);

      _timeInCurrentState += gameTime.ElapsedGameTime.TotalSeconds;

      if (_isDrawing) {
        if (_timeInCurrentState > _blinkOnPeriod) {
          _timeInCurrentState -= _blinkOnPeriod;
          _isDrawing = false;
        }
      }
      else {
        if (_timeInCurrentState > _blinkOffPeriod) {
          _timeInCurrentState -= _blinkOffPeriod;
          _isDrawing = true;
        }
      }

    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      if (_isDrawing) {
        var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
        _child.Draw(spriteBatch, transformationMatrix);
      }
    }
  }
}
