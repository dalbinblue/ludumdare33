﻿using System;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class FixedSizeTileset {
    public int TileWidth { get; private set; }
    public int TileHeight { get; private set; }
    private int _tilesPerRow;
    private int _tileCount;
    private int _margin;
    private int _spacing;

    public Texture2D Texture { get; private set; }

    protected FixedSizeTileset() {
       
    }

    public static FixedSizeTileset FromTileMapTilesetDefinition(TileMapTilesetDefinition tilesetDefinition, ContentManager content) {
      var tileset = new FixedSizeTileset();

      tileset.Texture = content.Load<Texture2D>(tilesetDefinition.ImageAssetName);
      tileset.TileHeight = tilesetDefinition.TileHeight;
      tileset.TileWidth = tilesetDefinition.TileWidth;
      tileset._margin = tilesetDefinition.Margin;
      tileset._spacing = tilesetDefinition.Spacing;

      tileset._tilesPerRow = (tileset.Texture.Width - (tileset._margin * 2) + tileset._spacing) 
        / (tileset.TileWidth + tileset._spacing);

      int tilesPerColumn = (tileset.Texture.Height - (tileset._margin * 2) + tileset._spacing) 
        / (tileset.TileHeight + tileset._spacing);

      tileset._tileCount = tileset._tilesPerRow * tilesPerColumn;

      return tileset;      
    }

    public static FixedSizeTileset FromTexture(Texture2D texture, int tileWidth, int tileHeight) {
      var tileset = new FixedSizeTileset();

      tileset.Texture = texture;
      tileset.TileHeight = tileHeight;
      tileset.TileWidth = tileWidth;
      tileset._margin = 0;
      tileset._spacing = 0;

      tileset._tilesPerRow = texture.Width / tileset.TileWidth;

      int tilesPerColumn = texture.Height / tileset.TileHeight;

      tileset._tileCount = tileset._tilesPerRow * tilesPerColumn;

      return tileset;
    }

    public Rectangle GetTileBounds(int tileId) {
      if (tileId < 0 || tileId >= _tileCount) {
        throw new ArgumentOutOfRangeException(
          "tileId",
          tileId,
          string.Format(
            "TileIndex must be 0 or greater and less than TileCount ({0}).",
            _tileCount));
      }

      int tileX = tileId % _tilesPerRow;
      int tileY = tileId / _tilesPerRow;

      return new Rectangle(
        tileX * (TileWidth + _spacing) + _margin, 
        tileY * (TileHeight + _spacing) + _margin, 
        TileWidth, 
        TileHeight);
    }
  }
}
