using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public interface IPositionedObject {
    Vector2 Position { get; }
  }

    public interface IYOrderedObject
    {
        float Bottom { get; }
    }
}