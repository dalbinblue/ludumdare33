﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class Layer : RenderableObjectBase {
    protected readonly List<IRenderableObject> Children = new List<IRenderableObject>();
    private bool _orderByYCoordinate = true;

    public bool IsHudLayer { get; set; }

    public override void PreMovementUpdate(GameTime gameTime) {
      var children = new List<IRenderableObject>(Children);

      foreach (var renderableObject in children)
      {
        renderableObject.Update(gameTime);
      }
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;

      IEnumerable<IRenderableObject> orderedChilden;
      if (OrderByYCoordinate) {
        orderedChilden = Children.OrderBy(c => c is IYOrderedObject ? ((IYOrderedObject)c).Bottom : c.Position.Y);
      }
      else {
        orderedChilden = Children;
      }

      foreach (IRenderableObject renderableObject in orderedChilden) {
        renderableObject.Draw(spriteBatch, transformationMatrix);
      }
    }

    public bool OrderByYCoordinate {
      get { return _orderByYCoordinate; }
      set { _orderByYCoordinate = value; }
    }

    public void Add(IRenderableObject renderableObject) {
      Children.Add(renderableObject);
    }

    public void Remove(IRenderableObject renderableObject) {
      Children.Remove(renderableObject);
    }

    public void Clear() {
      Children.Clear();
    }

    public BlendState DesiredBlendState { get; set; }

    public bool Contains(IRenderableObject item) {
      return Children.Contains(item);
    }
  }
}
