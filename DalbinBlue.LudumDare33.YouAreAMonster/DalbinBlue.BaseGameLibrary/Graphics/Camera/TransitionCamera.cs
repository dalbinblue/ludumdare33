﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics.Camera {
  public class TransitionCamera : ICamera {
    private readonly EasingFunc _defaultEasingFunction = Easing.QuadraticEasing;

    private bool _isRotationAnimating;
    private float _originalRotation;
    private float _targetRotation;
    private float _rotationAnimationDuration;
    private float _rotationCurrentTime;
    private EasingFunc _rotationEasingFunction;
    private string _rotationAnimationName;

    private bool _isScaleAnimating;
    private float _originalScale;
    private float _targetScale;
    private float _scaleAnimationDuration;
    private float _scaleCurrentTime;
    private EasingFunc _scaleEasingFunction;
    private string _scaleAnimationName;

    private bool _isTrackingObjectAnimating;
    private Vector2 _originalTrackingObjectPosition;
    private IPositionedObject _targetTrackingObject;
    private float _trackingObjectAnimationDuration;
    private float _trackingObjectCurrentTime;
    private EasingFunc _trackingObjectEasingFunction;
    private Vector2 _currentTrackingPosition;
    private string _positionAnimationName;

    private bool _isOffsetAnimating;
    private Point _originalOffset;
    private Point _targetOffset;
    private float _offsetAnimationDuration;
    private float _offsetCurrentTime;
    private EasingFunc _offsetEasingFunction;
    private Vector2 _currentOffset;
    private string _offsetAnimationName;

    public int ViewPortWidth { get; set; }
    public int ViewPortHeight { get; set; }
    public int SceneWidth { get; set; }
    public int SceneHeight { get; set; }
    public Point SceneOffset { get; set; }
    public bool ClampPositionToScene { get; set; }

    public float Rotation { get; private set; }
    public float Zoom { get; private set; }
    public float X { get; private set; }
    public float Y { get; private set; }

    public Point TargetOffset {
      get { return _targetOffset; }
    }


    public event TweeningAnimationEventHandler TweeningAnimationComplete;

    protected virtual void OnTweeningAnimationComplete(TweeningAnimationEventHandlerArgs args) {
      TweeningAnimationEventHandler handler = TweeningAnimationComplete;
      if (handler != null) handler(this, args);
    }



    public TransitionCamera() {
      SetRotation(0.0f);
      SetScale(1.0f);
      SetTarget(new Point(0, 0));
      SetCameraOffset(new Point(0, 0));
      ViewPortWidth = int.MaxValue;
      ViewPortHeight = int.MaxValue;
      SceneWidth = int.MaxValue;
      SceneHeight = int.MaxValue;
      SceneOffset = new Point(0,0);
    }



    public void SetRotation(float rotation) {
      Rotation = rotation;
      _isRotationAnimating = false;
      _originalRotation = rotation;
      _targetRotation = rotation;
      _rotationAnimationDuration = 0.0f;
      _rotationCurrentTime = 0.0f;
      _rotationEasingFunction = null;
      SignalScriptsWaitingForRotationToComplete();
    }

    public void SetScale(float scale) {
      Zoom = scale;
      _isScaleAnimating = false;
      _originalScale = scale;
      _targetScale = scale;
      _scaleAnimationDuration = 0.0f;
      _scaleCurrentTime = 0.0f;
      _scaleEasingFunction = null;
      SignalScriptsWaitingForScaleToComplete();
    }

    public void SetTarget(IPositionedObject target) {
      _isTrackingObjectAnimating = false;
      _originalTrackingObjectPosition = new Vector2(X, Y);
      _currentTrackingPosition = new Vector2(X, Y);
      _targetTrackingObject = target;
      _trackingObjectAnimationDuration = 0.0f;
      _trackingObjectCurrentTime = 0.0f;
      _trackingObjectEasingFunction = null;
      SignalScriptsWaitingForTrackingObjectToComplete();
    }

    public void SetTarget(Point target) {
      SetTarget(new StaticPositionedObject(target.ToVector()));
    }

    public void SetCameraOffset(Point cameraOffset) {
      _isOffsetAnimating = false;
      _originalOffset = cameraOffset;
      _targetOffset = cameraOffset;
      _currentOffset = cameraOffset.ToVector();
      _offsetAnimationDuration = 0.0f;
      _offsetCurrentTime = 0.0f;
      _offsetEasingFunction = null;
      SignalScriptsWaitingForOffsetToComplete();
    }



    public void AnimateRotationBySpeed(float targetRotation, float speed) {
      AnimateRotationBySpeed(targetRotation, speed, _defaultEasingFunction, null);
    }

    public void AnimateRotationBySpeed(float targetRotation, float speed, EasingFunc easing) {
      AnimateRotationBySpeed(targetRotation, speed, easing, null);
    }

    public void AnimateRotationBySpeed(float targetRotation, float speed, EasingFunc easing, string animationName) {
      float rotationDistance = GetMinimalRotationAmount(Rotation, targetRotation);
      AnimateRotation(targetRotation, rotationDistance/speed, easing, animationName);
    }

    public void AnimateRotation(float targetRotation, float duration) {
      AnimateRotation(targetRotation, duration, _defaultEasingFunction, null);
    }

    public void AnimateRotation(float targetRotation, float duration, EasingFunc easing) {
      AnimateRotation(targetRotation, duration, easing, null);
    }

    public void AnimateRotation(float targetRotation, float duration, EasingFunc easing, string animationName) {
      _originalRotation = GetMinimalOriginalRotationToTarget(Rotation, targetRotation);
      _targetRotation = targetRotation;
      _rotationAnimationDuration = duration;
      _rotationCurrentTime = 0;
      _isRotationAnimating = true;
      _rotationEasingFunction = easing;
      _rotationAnimationName = animationName;
      SignalScriptsWaitingForRotationToComplete();
    }

    private float GetMinimalOriginalRotationToTarget(float originalRotation, float targetRotation) {
      float currentDistance = Math.Abs(targetRotation - originalRotation);
      float rotatedDownDistance = Math.Abs(targetRotation - (originalRotation - MathHelper.TwoPi));
      float rotatedUpDistance = Math.Abs(targetRotation - (originalRotation + MathHelper.TwoPi));

      if (currentDistance < rotatedDownDistance) {
        if (currentDistance < rotatedUpDistance) {
          return originalRotation;
        }
        return originalRotation + MathHelper.TwoPi;
      }
      if (rotatedDownDistance < rotatedUpDistance) {
        return originalRotation - MathHelper.TwoPi;
      }
      return originalRotation + MathHelper.TwoPi;
    }

    private float GetMinimalRotationAmount(float originalRotation, float targetRotation) {
      return Math.Abs(targetRotation - GetMinimalOriginalRotationToTarget(originalRotation, targetRotation));
    }



    public void AnimateScaleBySpeed(float targetScale, float speed) {
      AnimateScaleBySpeed(targetScale, speed, _defaultEasingFunction, null);
    }

    public void AnimateScaleBySpeed(float targetScale, float speed, EasingFunc easing) {
      AnimateScaleBySpeed(targetScale, speed, easing, null);
    }

    public void AnimateScaleBySpeed(float targetScale, float speed, EasingFunc easing, string animationName) {
      AnimateScale(targetScale, Math.Abs(Zoom - targetScale)/speed, easing, animationName);
    }

    public void AnimateScale(float targeScale, float duration) {
      AnimateScale(targeScale, duration, _defaultEasingFunction, null);
    }

    public void AnimateScale(float targetScale, float duration, EasingFunc easing) {
      AnimateScale(targetScale, duration, easing, null);
    }

    public void AnimateScale(float targetScale, float duration, EasingFunc easing, string animationName) {
      _isScaleAnimating = true;
      _originalScale = Zoom;
      _targetScale = targetScale;
      _scaleAnimationDuration = duration;
      _scaleCurrentTime = 0.0f;
      _scaleEasingFunction = easing;
      _scaleAnimationName = animationName;
      SignalScriptsWaitingForScaleToComplete();
    }



    public void AnimateToTarget(IPositionedObject target, float duration) {
      AnimateToTarget(target, duration, _defaultEasingFunction, null);
    }

    public void AnimateToTarget(IPositionedObject target, float duration, EasingFunc easing) {
      AnimateToTarget(target, duration, easing, null);
    }

    public void AnimateToTarget(IPositionedObject target, float duration, EasingFunc easing, string animationName) {
      _isTrackingObjectAnimating = true;
      _originalTrackingObjectPosition = _currentTrackingPosition;
      _targetTrackingObject = target;
      _trackingObjectAnimationDuration = duration;
      _trackingObjectCurrentTime = 0.0f;
      _trackingObjectEasingFunction = easing;
      _positionAnimationName = animationName;
      SignalScriptsWaitingForTrackingObjectToComplete();
    }

    public void AnimateToTargetBySpeed(IPositionedObject target, float speed) {
      AnimateToTargetBySpeed(target, speed, _defaultEasingFunction, null);
    }

    public void AnimateToTargetBySpeed(IPositionedObject target, float speed, EasingFunc easing) {
      AnimateToTargetBySpeed(target, speed, easing, null);
    }

    public void AnimateToTargetBySpeed(IPositionedObject target, float speed, EasingFunc easing, string animationName) {
      float horizontalDistance = Math.Abs(target.Position.X - _targetTrackingObject.Position.X);
      float verticalDistance = Math.Abs(target.Position.Y - _targetTrackingObject.Position.Y);
      var maxDistance = Math.Max(horizontalDistance, verticalDistance);
      AnimateToTarget(target,maxDistance / speed, easing, animationName);
    }



    public void AnimateToPosition(Point target, float duration) {
      AnimateToTarget(new StaticPositionedObject(target.ToVector()), duration, _defaultEasingFunction, null);
    } 

    public void AnimateToPosition(Point target, float duration, EasingFunc easing) {
      AnimateToTarget(new StaticPositionedObject(target.ToVector()), duration, easing, null);
    }

    public void AnimateToPosition(Point target, float duration, EasingFunc easing, string animationName) {
      AnimateToTarget(new StaticPositionedObject(target.ToVector()), duration, easing, animationName);
    }

    public void AnimateToPositionBySpeed(Point target, float speed) {
      AnimateToTargetBySpeed(new StaticPositionedObject(target.ToVector()), speed);
    }

    public void AnimateToPositionBySpeed(Point target, float speed, EasingFunc easing) {
      AnimateToTargetBySpeed(new StaticPositionedObject(target.ToVector()), speed, easing );
    }

    public void AnimateToPositionBySpeed(Point target, float speed, EasingFunc easing, string animationName) {
      AnimateToTargetBySpeed(new StaticPositionedObject(target.ToVector()), speed, easing, animationName);
    }



    public void AnimateOffset(Point offset, float duration) {
      AnimateOffset(offset,duration,_defaultEasingFunction,null);
    }

    public void AnimateOffset(Point offset, float duration, EasingFunc easing) {
      AnimateOffset(offset, duration, easing, null);
    }

    public void AnimateOffset(Point offset, float duration, EasingFunc easing, string animationName) {
      _isOffsetAnimating = true;
      _originalOffset = _currentOffset.ToPoint();
      _targetOffset = offset;
      _offsetAnimationDuration = duration;
      _offsetCurrentTime = 0.0f;
      _offsetEasingFunction = easing;
      _offsetAnimationName = animationName;
      SignalScriptsWaitingForOffsetToComplete();
    }

    public void AnimateOffsetBySpeed(Point offset, float speed) {
      AnimateOffsetBySpeed(offset,speed,_defaultEasingFunction,null);
    }

    public void AnimateOffsetBySpeed(Point offset, float speed, EasingFunc easing) {
      AnimateOffsetBySpeed(offset, speed, easing, null);
    }

    public void AnimateOffsetBySpeed(Point offset, float speed, EasingFunc easing, string animationName) {
      float horizontalDistance = Math.Abs(offset.X - _targetOffset.X);
      float verticalDistance = Math.Abs(offset.Y - _targetOffset.Y);
      var maxDistance = Math.Max(horizontalDistance, verticalDistance);
      AnimateOffset(offset, maxDistance / speed, easing, animationName);
    }

    public void Update(GameTime gameTime) {
      var secondsElapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

      if (_isRotationAnimating) {
        UpdateRotationAnimation(secondsElapsed);
      }
      
      if (_isScaleAnimating) {
        UpdateScaleAnimation(secondsElapsed);
      }
     
      if (_isOffsetAnimating) {
        UpdateOffsetAnimation(secondsElapsed);
      }
 
      if (_isTrackingObjectAnimating) {
        UpdateTrackingObjectAnimation(secondsElapsed);
      }
      else {
        _currentTrackingPosition = _targetTrackingObject.Position;
      }

      if (ClampPositionToScene) {
        X = MathHelper.Clamp(_currentTrackingPosition.X + _currentOffset.X, ViewPortWidth/2f + SceneOffset.X,
                             SceneWidth - (ViewPortWidth/2f) + SceneOffset.X);
        Y = MathHelper.Clamp(_currentTrackingPosition.Y + _currentOffset.Y, ViewPortHeight/2f + SceneOffset.Y,
                             SceneHeight - (ViewPortHeight/2f) + SceneOffset.Y);
      }
      else {
        X = _currentTrackingPosition.X;
        Y = _currentTrackingPosition.Y;
      }
    }

    private void UpdateTrackingObjectAnimation(float secondsElapsed) {
      _trackingObjectCurrentTime += secondsElapsed;

      if (_trackingObjectCurrentTime >= _trackingObjectAnimationDuration) {
        _isTrackingObjectAnimating = false;
        _currentTrackingPosition = _targetTrackingObject.Position;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Position, AnimationName = _positionAnimationName});
        SignalScriptsWaitingForTrackingObjectToComplete();
      }
      else {
        var percentAnimationComplete = _trackingObjectCurrentTime/_trackingObjectAnimationDuration;
        var newX = _trackingObjectEasingFunction(_originalTrackingObjectPosition.X, 
                                                 _targetTrackingObject.Position.X,
                                                 percentAnimationComplete);
        var newY = _trackingObjectEasingFunction(_originalTrackingObjectPosition.Y,
                                                 _targetTrackingObject.Position.Y,
                                                 percentAnimationComplete);
        _currentTrackingPosition = new Vector2(newX, newY);
      }
    }

    private void UpdateOffsetAnimation(float secondsElapsed) {
      _offsetCurrentTime += secondsElapsed;

      if (_offsetCurrentTime > _offsetAnimationDuration) {
        _isOffsetAnimating = false;
        _currentOffset = _targetOffset.ToVector();
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Offset, AnimationName = _offsetAnimationName});
        SignalScriptsWaitingForOffsetToComplete();
      }
      else {
        var percentAnimationComplete = _offsetCurrentTime/_offsetAnimationDuration;
        var newX = _offsetEasingFunction(_originalOffset.X,
                                         _targetOffset.X,
                                         percentAnimationComplete);
        var newY = _offsetEasingFunction(_originalOffset.Y,
                                         _targetOffset.Y,
                                         percentAnimationComplete);
        _currentOffset = new Vector2(newX, newY);        
      }      
    }

    private void UpdateScaleAnimation(float secondsElapsed) {
      _scaleCurrentTime += secondsElapsed;

      if (_scaleCurrentTime >= _scaleAnimationDuration) {
        _isScaleAnimating = false;
        Zoom = _targetScale;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Scale, AnimationName = _scaleAnimationName});
        SignalScriptsWaitingForScaleToComplete();
      }
      else {
        var percentAnimationComplete = _scaleCurrentTime/_scaleAnimationDuration;
        Zoom = _scaleEasingFunction(_originalScale, _targetScale, percentAnimationComplete);
      }
    }

    private void UpdateRotationAnimation(float secondsElapsed) {
      _rotationCurrentTime += secondsElapsed;

      if (_rotationCurrentTime >= _rotationAnimationDuration) {
        _isRotationAnimating = false;
        Rotation = _targetRotation;
        OnTweeningAnimationComplete(new TweeningAnimationEventHandlerArgs { AnimationType = TweenAnimationType.Rotation, AnimationName = _rotationAnimationName});
        SignalScriptsWaitingForRotationToComplete();
      }
      else {
        var percentAnimationComplete = _rotationCurrentTime/_rotationAnimationDuration;
        Rotation = _rotationEasingFunction(_originalRotation, _targetRotation, percentAnimationComplete);
      }
    }



    private List<ScriptState> _scriptsWaitingForRotationToComplete = new List<ScriptState>();
    public ScriptState WaitForRotationAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isRotationAnimating)
      {
        _scriptsWaitingForRotationToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForRotationToComplete()
    {
      if (_scriptsWaitingForRotationToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForRotationToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForRotationToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForScaleToComplete = new List<ScriptState>();
    public ScriptState WaitForScaleAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isScaleAnimating)
      {
        _scriptsWaitingForScaleToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForScaleToComplete()
    {
      if (_scriptsWaitingForScaleToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForScaleToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForScaleToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForOffsetToComplete = new List<ScriptState>();
    public ScriptState WaitForOffsetAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isOffsetAnimating)
      {
        _scriptsWaitingForOffsetToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForOffsetToComplete()
    {
      if (_scriptsWaitingForOffsetToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForOffsetToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForOffsetToComplete = new List<ScriptState>();
      }
    }



    private List<ScriptState> _scriptsWaitingForTrackingObjectToComplete = new List<ScriptState>();
    public ScriptState WaitForTrackingObjectAnimationToComplete()
    {
      var state = new ScriptState();
      if (_isTrackingObjectAnimating)
      {
        _scriptsWaitingForTrackingObjectToComplete.Add(state);
      }
      else
      {
        state.IsComplete = true;
      }
      return state;
    }

    private void SignalScriptsWaitingForTrackingObjectToComplete()
    {
      if (_scriptsWaitingForTrackingObjectToComplete.Any())
      {
        foreach (var state in _scriptsWaitingForTrackingObjectToComplete)
        {
          state.IsComplete = true;
        }
        _scriptsWaitingForTrackingObjectToComplete = new List<ScriptState>();
      }
    }
  }
}
