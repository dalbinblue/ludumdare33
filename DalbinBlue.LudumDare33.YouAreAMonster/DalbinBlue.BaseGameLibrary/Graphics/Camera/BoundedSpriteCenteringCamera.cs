using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics.Camera
{
  public class BoundedSpriteCenteringCamera : ICamera
  {
    public float Rotation { get; set; }
    public float Zoom { get; set; }

    public AnimatedSprite SpriteToFollow { get; set; }
    public int ViewPortWidth { get; set; }
    public int ViewPortHeight { get; set; }
    public int SceneWidth { get; set; }
    public int SceneHeight { get; set; }
    public Point OriginOffset { get; set; }

    public float X
    {
      get
      {
        return SpriteToFollow == null ? 0 : MathHelper.Clamp(SpriteToFollow.Position.X + OriginOffset.X, ViewPortWidth/2f, SceneWidth - (ViewPortWidth/2f));
      }
    }

    public float Y
    {
      get {
        return SpriteToFollow == null ? 0 : MathHelper.Clamp(SpriteToFollow.Position.Y + OriginOffset.Y, ViewPortHeight / 2f, SceneHeight - (ViewPortHeight / 2f));
      }
    }

    public BoundedSpriteCenteringCamera()
    {
      Rotation = 0f;
      Zoom = 1f;
    }

    public void Update(GameTime gameTime)
    {
      // DO NOTHING
    }
  }

  public class SpriteCenteringCamera : ICamera {
    public float Rotation { get; set; }
    public float Zoom { get; set; }

    public AnimatedSprite SpriteToFollow { get; set; }
    public Point OriginOffset { get; set; }

    public float X {
      get { return SpriteToFollow == null ? 0 : SpriteToFollow.Position.X + OriginOffset.X; }
    }

    public float Y {
      get { return SpriteToFollow == null ? 0 : SpriteToFollow.Position.Y + OriginOffset.Y; }
    }

    public SpriteCenteringCamera() {
      Rotation = 0f;
      Zoom = 1f;
    }

    public void Update(GameTime gameTime) {
      // DO NOTHING
    }
  }
}