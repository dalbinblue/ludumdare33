﻿using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public delegate float EasingFunc(float initialValue, float finalValue, float percentageThroughTransition);

  public static class Easing {
    public static float LinearEasing(float initialValue, float finalValue, float percentageThroughTransition) {
      percentageThroughTransition = MathHelper.Clamp(percentageThroughTransition, 0.0f, 1.0f);
      return initialValue + ((finalValue - initialValue)*percentageThroughTransition);
    }

    public static float QuadraticEasing(float initialValue, float finalValue, float percentageThroughTransition) {
      percentageThroughTransition = MathHelper.Clamp(percentageThroughTransition, 0.0f, 1.0f) * 2.0f;

      var change = finalValue - initialValue;

      if (percentageThroughTransition < 1) {
        return change / 2 * percentageThroughTransition * percentageThroughTransition + initialValue;
      }
      
      percentageThroughTransition -= 1;
      return (-change)/2*(percentageThroughTransition*(percentageThroughTransition - 2) - 1) + initialValue;
    }

    public static float QuadraticInEasing(float initialValue, float finalValue, float percentageThroughTransition) {
      percentageThroughTransition = MathHelper.Clamp(percentageThroughTransition, 0.0f, 1.0f);
      var change = finalValue - initialValue;
      return change*percentageThroughTransition*percentageThroughTransition + initialValue;
    }

    public static float QuadraticOutEasing(float initialValue, float finalValue, float percentageThroughTransition) {
      percentageThroughTransition = MathHelper.Clamp(percentageThroughTransition, 0.0f, 1.0f);
      var change = finalValue - initialValue;
      return -change * percentageThroughTransition * (percentageThroughTransition - 2.0f) + initialValue;
    }
  }
}
