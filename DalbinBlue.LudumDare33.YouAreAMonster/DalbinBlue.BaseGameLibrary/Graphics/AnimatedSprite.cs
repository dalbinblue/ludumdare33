﻿using System;
using System.Collections.Generic;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Input;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics
{
  public class AnimatedSprite : RenderableObjectBase, IYOrderedObject
  {
    private SpriteSheet _spriteSheet;
    private SpriteDefinition _definition;
    private SpriteAnimationDefinition _currentAnimation;
    private Texture2D _texture;

    private SpriteFrame _currentFrame;
    private int _currentFrameIndex;
    private double _secondsOnCurrentFrame;

    private bool _animationPaused;
    private bool _triggeredEvent;
    private List<ScriptState> _scriptsWaitingForAnimationToComplete = new List<ScriptState>(); 

    public string CurrentAnimationName { get; protected set; }
    public bool FlipHorizontally { get; set; }
    public bool FlipVertically { get; set; }

    //public int Rotation { get; set; }

    public delegate void SpriteAnimationEndedHandler(object sender, SpriteAnimageEndedEventArgs args);

    public event SpriteAnimationEndedHandler SpriteAnimationEnded;

    public void OnSpriteAnimationEnded(SpriteAnimageEndedEventArgs args)
    {
      SpriteAnimationEndedHandler handler = SpriteAnimationEnded;
      if (handler != null) handler(this, args);
    }

    protected AnimatedSprite()
    {
        // Use FromSpriteSheet
    }

    public static AnimatedSprite FromSpriteSheet(SpriteSheet spriteSheet, string spriteName, ContentManager content)
    {
      var definition = spriteSheet.Sprites[spriteName];

      var sprite = new AnimatedSprite
                   {
                     _definition = definition, 
                     _spriteSheet = spriteSheet,
                     _texture = content.Load<Texture2D>(spriteSheet.ImageAssetName)
                   };

      if (definition.DefaultAnimation != null)
      {
        sprite.SetAnimation(definition.DefaultAnimation);
      }

      return sprite;
    }
        
    public Rectangle GetCollisionBounds()
    {
      SpriteImageSlice slice = _spriteSheet.ImageSlices[_currentFrame.Slice];

      var collisionBounds = new Rectangle();

      if (FlipHorizontally)
      {
        collisionBounds.X = (int)Position.X + slice.Origin.X - slice.CollisionBounds.X - slice.CollisionBounds.Width;
      }
      else
      {
        collisionBounds.X = (int)Position.X - slice.Origin.X + slice.CollisionBounds.X;        
      }

      if (FlipVertically)
      {
        collisionBounds.Y = (int)Position.Y + slice.Origin.Y - slice.CollisionBounds.Y - slice.CollisionBounds.Height;
      }
      else
      {
        collisionBounds.Y = (int)Position.Y - slice.Origin.Y + slice.CollisionBounds.Y;        
      }

      collisionBounds.Width = slice.CollisionBounds.Width;
      collisionBounds.Height = slice.CollisionBounds.Height;

      return collisionBounds;
    }

    public void SetAnimation(string animationName)
    {
        SetAnimation(animationName, false);
    }

    public void SetAnimation(string animationName, bool forceRetartIfSameAsCurrent)
    {
      if (!forceRetartIfSameAsCurrent && animationName == CurrentAnimationName) return;
      _triggeredEvent = false;
      _currentAnimation = _definition.Animations[animationName];
      CurrentAnimationName = animationName;
      _currentFrameIndex = _currentAnimation.StartFrame;
      _currentFrame = _currentAnimation.Frames[0];
      _secondsOnCurrentFrame = 0.0;
      foreach (var state in _scriptsWaitingForAnimationToComplete)
      {
        state.IsComplete = true;
      }
      _scriptsWaitingForAnimationToComplete = new List<ScriptState>();
    }

    public void PauseAnimation()
    {
      _animationPaused = true;
    }

    public void ResumeAnimation()
    {
      _animationPaused = false;
    }

    public bool IsAnimationPaused()
    {
      return _animationPaused;
    }

    public override void PostMovementUpdate(GameTime gameTime)
    {
      SpriteAnimageEndedEventArgs eventArgs = null;

      if (_animationPaused) return;

      double secondsSinceLastUpdate = gameTime.ElapsedGameTime.TotalSeconds;
      var lastFrameIndex = _currentAnimation.Frames.Length - 1;
      _secondsOnCurrentFrame += secondsSinceLastUpdate;

      while (_secondsOnCurrentFrame > _currentFrame.FrameLength) {

        _secondsOnCurrentFrame -= _currentFrame.FrameLength;
        _currentFrameIndex += 1;

        if (_currentFrameIndex > lastFrameIndex) {
          if (!_currentAnimation.Loop)
          {
            eventArgs = new SpriteAnimageEndedEventArgs { AnimationName = CurrentAnimationName };
            if (!_triggeredEvent)
            {
              _triggeredEvent = true;
            }

            if (_currentAnimation.JumpToAnimationOnEnd != null)
            {
              SetAnimation(_currentAnimation.JumpToAnimationOnEnd);
              break;
            }
          }

          _currentFrameIndex = _currentAnimation.Loop
                                 ? _currentAnimation.StartFrameOnLoop
                                 : lastFrameIndex;
        }

        _currentFrame = _currentAnimation.Frames[_currentFrameIndex];

        if (eventArgs != null)
        {
          foreach (var state in _scriptsWaitingForAnimationToComplete)
          {
            state.IsComplete = true;
          }
          _scriptsWaitingForAnimationToComplete = new List<ScriptState>();
          OnSpriteAnimationEnded(eventArgs);
        }
      }
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      if (CurrentAnimationName == null) return;

      var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
      TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      SpriteImageSlice slice = _spriteSheet.ImageSlices[_currentFrame.Slice];

      var origin =
        new Vector2(FlipHorizontally ? slice.ImageBounds.Width - slice.Origin.X : slice.Origin.X, slice.Origin.Y).Shift(
          Origin);

      spriteBatch.Draw(
        _texture,
        transformationData.Position, 
        slice.ImageBounds,
        Color,
        transformationData.Rotation,
        origin,
        transformationData.Scale,
        (FlipHorizontally ? SpriteEffects.FlipHorizontally : SpriteEffects.None) | (FlipVertically ? SpriteEffects.FlipVertically : SpriteEffects.None), 
        Layer);
    }

    public bool HasAnimation(string animationName)
    {
      return _definition.Animations.ContainsKey(animationName);
    }

    public ScriptState WaitForAnimationToComplete()
    {
      var state = new ScriptState();
     _scriptsWaitingForAnimationToComplete.Add(state);
      return state;
    }

     public float Bottom => GetCollisionBounds().Bottom;
    }

    public class SpriteAnimageEndedEventArgs : EventArgs
  {
    public string AnimationName { get; set; }
  }
}
